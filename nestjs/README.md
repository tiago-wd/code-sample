```bash

cp .env.example .env

docker-compose up --build

docker-compose exec --user=node nodejs npm run typeorm migration:run

```