import {MigrationInterface, QueryRunner} from "typeorm";

export class conteudoTable1552572346927 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "conteudos" ("id" SERIAL NOT NULL, "nome" character varying NOT NULL, "tipo" character varying NOT NULL, "caminho" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "id_spot" integer NOT NULL, CONSTRAINT "PK_56cc3b3344c007f6378440afd1f" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "conteudos" ADD CONSTRAINT "FK_26cb87f5ede66d5ecec90451468" FOREIGN KEY ("id_spot") REFERENCES "spots"("id") ON DELETE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "conteudos" DROP CONSTRAINT "FK_26cb87f5ede66d5ecec90451468"`);
        await queryRunner.query(`DROP TABLE "conteudos"`);
    }

}
