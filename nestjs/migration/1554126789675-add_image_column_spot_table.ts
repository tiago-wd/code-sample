import {MigrationInterface, QueryRunner} from "typeorm";

export class addImageColumnSpotTable1554126789675 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "spots" ADD "imagem_perfil" character varying`);
        await queryRunner.query(`ALTER TABLE "spots" ADD "imagem_capa" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "spots" DROP COLUMN "imagem_capa"`);
        await queryRunner.query(`ALTER TABLE "spots" DROP COLUMN "imagem_perfil"`);
    }

}
