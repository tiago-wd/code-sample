import {MigrationInterface, QueryRunner} from "typeorm";

export class AddGeoPontoSpot1555945491590 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "spots" ADD "geo_ponto" geography(Point,4326)`);
        await queryRunner.query(`CREATE INDEX "IDX_5b9b871239f2f6dda5b215d0b5" ON "spots" USING GiST ("geo_ponto") `);
        await queryRunner.query(`CREATE INDEX "IDX_125649d077c80388defca6916c" ON "usuarios_spots" ("id_usuario") `);
        await queryRunner.query(`CREATE INDEX "IDX_7f50d77f71989db5b3e1a32bf5" ON "usuarios_spots" ("id_spot") `);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP INDEX "IDX_7f50d77f71989db5b3e1a32bf5"`);
        await queryRunner.query(`DROP INDEX "IDX_125649d077c80388defca6916c"`);
        await queryRunner.query(`DROP INDEX "IDX_5b9b871239f2f6dda5b215d0b5"`);
        await queryRunner.query(`ALTER TABLE "spots" DROP COLUMN "geo_ponto"`);
    }

}
