import {MigrationInterface, QueryRunner} from "typeorm";

export class alteracaoTabelaConfiguracoesSpots1553883427670 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "configuracoes_spots" ADD "valor" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "configuracoes_spots" ADD "created_at" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "configuracoes_spots" ADD "updated_at" TIMESTAMP NOT NULL DEFAULT now()`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "configuracoes_spots" DROP COLUMN "updated_at"`);
        await queryRunner.query(`ALTER TABLE "configuracoes_spots" DROP COLUMN "created_at"`);
        await queryRunner.query(`ALTER TABLE "configuracoes_spots" DROP COLUMN "valor"`);
    }

}
