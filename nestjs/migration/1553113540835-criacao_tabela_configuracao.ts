import { MigrationInterface, QueryRunner } from "typeorm";

export class criacaoTabelaConfiguracao1553113540835 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "configuracoes" 
        ("id" SERIAL NOT NULL, "nome" character varying NOT NULL, "tipo" character varying NOT NULL, 
        "descritivo_pai" character varying NOT NULL, "tamanho" character varying NOT NULL, "opcao_padrao" 
        character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP 
        NOT NULL DEFAULT now(), CONSTRAINT "PK_7640d21fdc17722366904769d9e" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "configuracoes_spots" ("id_configuracao" integer NOT NULL, 
        "id_spot" integer NOT NULL, CONSTRAINT "PK_4a923dcb250d62cec0d2c1454fc" PRIMARY KEY ("id_configuracao", "id_spot"))`);
        await queryRunner.query(`ALTER TABLE "configuracoes_spots" ADD CONSTRAINT "FK_fdd9ffec2920d84c4b9b06d1e59" 
        FOREIGN KEY ("id_configuracao") REFERENCES "configuracoes"("id") ON DELETE CASCADE`);
        await queryRunner.query(`ALTER TABLE "configuracoes_spots" ADD CONSTRAINT "FK_b309952fceb6a8ab942ab8a7406" 
        FOREIGN KEY ("id_spot") REFERENCES "spots"("id") ON DELETE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "configuracoes_spots" DROP CONSTRAINT "FK_b309952fceb6a8ab942ab8a7406"`);
        await queryRunner.query(`ALTER TABLE "configuracoes_spots" DROP CONSTRAINT "FK_fdd9ffec2920d84c4b9b06d1e59"`);
        await queryRunner.query(`DROP TABLE "configuracoes_spots"`);
        await queryRunner.query(`DROP TABLE "configuracoes"`);
    }

}
