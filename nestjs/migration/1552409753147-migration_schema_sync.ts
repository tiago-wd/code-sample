import { MigrationInterface, QueryRunner } from "typeorm";

export class migrationSchemaSync1552409753147 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const tableUsuarios = await queryRunner.query(`SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = 'usuarios')`);
        const tableSpots = await queryRunner.query(`SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = 'spots')`);
        const tableUsuariosSpots = await queryRunner.query(`SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = 'usuarios_spots')`);
        if (!tableUsuarios[0].exists &&
            !tableSpots[0].exists &&
            !tableUsuariosSpots[0].exists) {
            await queryRunner.query(`CREATE TABLE IF NOT EXISTS "usuarios" ("id" SERIAL NOT NULL, "nome" character varying NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "UQ_446adfc18b35418aac32ae0b7b5" UNIQUE ("email"), CONSTRAINT "PK_d7281c63c176e152e4c531594a8" PRIMARY KEY ("id"))`);
            await queryRunner.query(`CREATE TABLE IF NOT EXISTS "spots" ("id" SERIAL NOT NULL, "nome" character varying NOT NULL, "descricao" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "id_usuario" integer NOT NULL, CONSTRAINT "PK_cc8c0341ef60619746e42815cf4" PRIMARY KEY ("id"))`);
            await queryRunner.query(`CREATE TABLE IF NOT EXISTS "usuarios_spots" ("id_usuario" integer NOT NULL, "id_spot" integer NOT NULL, CONSTRAINT "PK_50950bd843a821c5cdcb9975ef7" PRIMARY KEY ("id_usuario", "id_spot"))`);
            await queryRunner.query(`ALTER TABLE "spots" ADD CONSTRAINT "FK_cb0f64b26b8fe4a17f39239651f" FOREIGN KEY ("id_usuario") REFERENCES "usuarios"("id") ON DELETE CASCADE`);
            await queryRunner.query(`ALTER TABLE "usuarios_spots" ADD CONSTRAINT "FK_125649d077c80388defca6916c3" FOREIGN KEY ("id_usuario") REFERENCES "usuarios"("id") ON DELETE CASCADE`);
            await queryRunner.query(`ALTER TABLE "usuarios_spots" ADD CONSTRAINT "FK_7f50d77f71989db5b3e1a32bf50" FOREIGN KEY ("id_spot") REFERENCES "spots"("id") ON DELETE CASCADE`);

        }
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "usuarios_spots" DROP CONSTRAINT "FK_7f50d77f71989db5b3e1a32bf50"`);
        await queryRunner.query(`ALTER TABLE "usuarios_spots" DROP CONSTRAINT "FK_125649d077c80388defca6916c3"`);
        await queryRunner.query(`ALTER TABLE "spots" DROP CONSTRAINT "FK_cb0f64b26b8fe4a17f39239651f"`);
        await queryRunner.query(`DROP TABLE "usuarios_spots"`);
        await queryRunner.query(`DROP TABLE "spots"`);
        await queryRunner.query(`DROP TABLE "usuarios"`);
    }

}
