import { MigrationInterface, QueryRunner } from "typeorm";

export class insercaoConfiguracoesPadrao1554834310295 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`INSERT INTO configuracoes(nome, tipo, opcao_padrao)
                                 VALUES ('Privacidade', 'string', 'Público')`);

        await queryRunner.query(`INSERT INTO configuracoes(nome, tipo, opcao_padrao)
                                VALUES ('Participantes Anônimos', 'boolean', 'Não')`);

        await queryRunner.query(`INSERT INTO configuracoes(nome, tipo, opcao_padrao)
                                VALUES ('Chat entre Participantes', 'boolean', 'Sim')`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
