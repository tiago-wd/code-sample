import {MigrationInterface, QueryRunner} from "typeorm";

export class permitirNullabelCamposConfiguracao1554232683148 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "configuracoes" ALTER COLUMN "descritivo_pai" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "configuracoes" ALTER COLUMN "tamanho" DROP NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "configuracoes" ALTER COLUMN "tamanho" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "configuracoes" ALTER COLUMN "descritivo_pai" SET NOT NULL`);
    }

}
