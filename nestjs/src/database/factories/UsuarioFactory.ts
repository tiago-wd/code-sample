import * as Faker from 'faker';
import { define } from 'typeorm-seeding';
import { Usuario } from '../../usuario/usuario.entity';

define(Usuario, (faker: typeof Faker, settings: { usuarioPadrao: boolean }) => {  

  const usuario = new Usuario();
  
  if (settings.usuarioPadrao)
  {
    usuario.nome = 'Soma Together';
    usuario.email = 'together@somatogether.com.br';
    usuario.password = '123321';
  }
  else
  {
    const gender = faker.random.number(1);
    const primeiroNome = faker.name.firstName(gender);
    const ultimoNome = faker.name.lastName(gender);
    const email = faker.internet.email(primeiroNome, ultimoNome).toLowerCase();
    const password = '123321';
    usuario.nome = primeiroNome + ' ' + ultimoNome;
    usuario.email = email;
    usuario.password = password;
  }  
  
  return usuario;
});