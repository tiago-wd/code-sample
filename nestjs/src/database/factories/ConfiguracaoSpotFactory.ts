import * as Faker from 'faker';
import { define } from 'typeorm-seeding';
import { ConfiguracaoSpot } from '../../configuracao_spot/configuracao_spot.entity';

define(ConfiguracaoSpot, (faker: typeof Faker, settings: { id_configuracao: number, id_spot: number }) => {

  let valor: string;
  if (settings.id_configuracao == 1) {
    valor = faker.random.arrayElement(['Público', 'Privado']);
  } else {
    valor = faker.random.arrayElement(['Sim', 'Não']);
  }

  const configuracaoSpot = new ConfiguracaoSpot();
  configuracaoSpot.id_configuracao = settings.id_configuracao;
  configuracaoSpot.id_spot = settings.id_spot;
  configuracaoSpot.valor = valor;

  return configuracaoSpot;
});