import * as Faker from 'faker';
import { define } from 'typeorm-seeding';
import { Spot } from '../../spot/spot.entity';

define(Spot, (faker: typeof Faker, settings: { roles: string[] }) => {
  // const nome = faker.company.companyName();
  // const descricao = faker.company.catchPhrase();    

  const spot = new Spot();
  spot.imagem_capa = 'public/spot-capa.jpg';
  spot.imagem_perfil = 'public/spot-perfil.png';
  return spot;
});