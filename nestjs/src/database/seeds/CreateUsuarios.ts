import { Factory, Seed } from 'typeorm-seeding';
import { Connection } from 'typeorm/connection/Connection';
import { Usuario } from '../../usuario/usuario.entity';

export class CreateUsuarios implements Seed {

    public async seed(factory: Factory, connection: Connection): Promise<any> {
        await factory(Usuario)({ usuarioPadrao: false }).seedMany(15);
        if (await connection.manager.count(Usuario, { where: { email: 'together@somatogether.com.br' } }) === 0) {
            await factory(Usuario)({ usuarioPadrao: true }).seed();
        }
    }

}