import { Factory, Seed } from 'typeorm-seeding';
import { Connection } from 'typeorm/connection/Connection';
import { Spot } from '../../spot/spot.entity';
import { Usuario } from '../../usuario/usuario.entity';
import { Configuracao } from '../../configuracao/configuracao.entity';
import { ConfiguracaoSpot } from '../../configuracao_spot/configuracao_spot.entity';
import * as wkx from 'wkx';

export class CreateSpots implements Seed {

  public async seed(factory: Factory, connection: Connection): Promise<any> {    

    let i = 0;
    const spots = await factory(Spot)()
      .map(async (spot: Spot) => {
        const locais = [
          { 
            nome: 'Padaria Doce Momento', latitude: -22.332523, longitude: -49.072255,
            descricao: 'Panificadora serve gastronomia do cotidiano, com matinais, lanches em pães da casa, e doces de confeiteiro', 
          },
          { 
            nome: 'Padaria Copacabana', latitude: -22.328363, longitude: -22.328363, 
            descricao: 'Matinais, lanches caseiros em pães de padaria seleta e opções de salgados, doces e variedades de confeiteiro', 
          },
          { 
            nome: 'Balaio de Krishna', latitude: -22.326259, longitude: -49.066400,
            descricao: 'Almoço com entradas, saladas e pratos quentes com toque oriental, sucos naturais, opções veganas e clima zen', 
          },
          { 
            nome: 'Água Doce Cachaçaria', latitude: -22.332382, longitude: -49.059029,
            descricao: 'Cadeia de bares acolhedores tem cachaças seletas, pratos da cozinha brasileira e a descontração de confraria',
          },
          { 
            nome: 'Fritz', latitude: -22.323212 , longitude: -49.057409,
            descricao: 'Mesas externas, Música ao vivo, Ótimos coquetéis',
          },
          { 
            nome: 'Churrascaria Tuvalu', latitude: -22.329226, longitude: -49.046905,
            descricao: 'Buffet à vontade, Entrada com acessibilidade, Casual',
          },
          { 
            nome: 'Flipper Lanches', latitude: -22.340490, longitude: -49.051443,
            descricao: 'Cardápio com lanches tradicionais, vegetarianos e da casa, ambiente familiar e casual com parquinho infantil',
          },
          { 
            nome: 'Dona Maria', latitude: -22.342475, longitude: -49.059533,
            descricao: 'Noites animadas em boteco rústico e descontraído, com tira-gostos, bebidas alcoólicas e transmissão dos jogos',
          },
          { 
            nome: 'Bambina Pizzaria', latitude: -22.343497, longitude: -49.063943,
            descricao: 'Pizzaria oferece pizzas clássicas e especiais em amplo salão ou em ambiente externo com muito verde tropical',
          },
          { 
            nome: 'Habibs', latitude: -22.337612, longitude: -49.069897,
            descricao: 'Rede de fast-food árabe, popular pelas esfihas, tem um ambiente simples e informal também para grandes grupos',
          },
          { 
            nome: 'Amadeus Bar & Restaurante', latitude: -22.333950, longitude: -49.075197,
            descricao: 'Pratos fartos à la carte com variedade de comida brasileira, bebidas, drinques, sobremesas, ambiente familiar',
          },
          { 
            nome: 'Bar Da Nelma', latitude: -22.334952, longitude: -49.078866,
            descricao: 'Mesas externas, Entrada com acessibilidade, Aceita somente dinheiro',
          },
          { 
            nome: 'Armazén Bar', latitude: -22.326948, longitude: -49.080381,
            descricao: 'Bar com destaque às pingas de frutas, petiscos variados, em ambiente descontraído ao som de shows de rock',
          },
          { 
            nome: 'Churrascaria Baby Buffalo', latitude: -22.319901, longitude: -49.069963,
            descricao: 'Em ambiente aconchegante e informal, churrascaria oferece cardápio de peixes e massas em opções à la carte',
          },
          { 
            nome: 'Ragazzo', latitude: -22.338668, longitude: -49.050482,
            descricao: 'Rede de cantinas italianas, de ambiente descontraído e menu variado de massas, pizzas, lanches e sorvetes',
          },
        ]
        const local = locais[i++];
        
        spot.nome = local['nome'];
        spot.descricao = local['descricao'];
        spot.usuario = await factory(Usuario)({ usuarioPadrao : false }).seed();  
        spot.usuarios = await factory(Usuario)({ usuarioPadrao : false }).seedMany(15);              
        spot.geo_ponto = wkx.Geometry.parse(`POINT(${local['latitude']} ${local['longitude']})`).toGeoJSON();        

        return await spot;
      })
      .seedMany(15);
        
    const configuracoes = await connection.manager.find(Configuracao);

    spots.map(async (spot) => {
      configuracoes.map(async (configuracao) => {

        const configuracao_spot = await factory(ConfiguracaoSpot)({
          id_configuracao: configuracao.id,
          id_spot: spot.id,
        })          
        .seed();

        await connection.manager.save(configuracao_spot);
      });
    });
  }

}