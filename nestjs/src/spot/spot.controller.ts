import { FileInterceptor } from '@nestjs/platform-express';
import { Get, Controller, Res, Post, UseInterceptors, UploadedFile, Inject, Param, ParseIntPipe } from '@nestjs/common';
import { diskStorage } from 'multer';
import { SpotInput } from './spot.interface';
import { SpotService } from './spot.service';
import { Response } from 'src/common/response.interface';

@Controller('spots')
export class SpotController {

  constructor(@Inject(SpotService) private readonly spotService: SpotService) { }

  @Get('audio')
  audio(@Res() res) {
    return res.download('./public/audio.mp3');
  }

  @Post(':id/imagem_capa')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: 'public',
        filename: (req, file, cb) => {
          const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
          return cb(null, `${randomName}${(file.originalname)}`)
        }
      })
    })
  )
  async imagem_capa(
    @UploadedFile() file,
    @Param('id', new ParseIntPipe()) id
  ): Promise<Response> {
    const updateSpotInput: SpotInput = {
      id: id,
      imagem_capa: file.path,
    }
    await this.spotService.update(updateSpotInput);
    return { code: 200, message: 'Spot atualizado com sucesso' };
  }

  @Post(':id/imagem_perfil')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: 'public',
        filename: (req, file, cb) => {
          const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
          return cb(null, `${randomName}${(file.originalname)}`)
        }
      })
    })
  )
  async imagem_perfil(
    @UploadedFile() file,
    @Param('id', new ParseIntPipe()) id
  ): Promise<Response> {
    const updateSpotInput: SpotInput = {
      id: id,
      imagem_perfil: file.path,
    }
    await this.spotService.update(updateSpotInput);
    return { code: 200, message: 'Spot atualizado com sucesso' };
  }
}