import { ConfiguracaoSpotInput } from '../configuracao_spot/configuracao_spot.interface';
import { Geography } from 'geojson';

export interface SpotInput {
  id?: number;
  nome?: string;
  descricao?: string;
  id_usuario?: number;
  imagem_perfil?: string;
  imagem_capa?: string;
  latitude?: number;
  longitude?: number;
  geo_ponto?: Geography;
  configuracoes?: [ConfiguracaoSpotInput];
}