import { PubSub } from 'graphql-subscriptions';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Spot } from './spot.entity';
import { SpotResolver } from './spot.resolver';
import { SpotService } from './spot.service';
import { SpotController } from './spot.controller';
import { Usuario } from '../usuario/usuario.entity';
import { UsuarioModule } from '../usuario/usuario.module';
import { ConfiguracaoSpot } from '../configuracao_spot/configuracao_spot.entity';
import { ConfiguracaoSpotModule } from '../configuracao_spot/configuracao_spot.module';

@Module({
  controllers: [SpotController],
  imports: [
    TypeOrmModule.forFeature([Spot, Usuario, ConfiguracaoSpot]),
    UsuarioModule,
    ConfiguracaoSpotModule
  ],
  providers: [SpotService, SpotResolver, PubSub],
  exports: [SpotService]
})

export class SpotModule { }
