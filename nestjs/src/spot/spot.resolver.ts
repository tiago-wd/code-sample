import { PaginationOptionsInterface } from './../common/paginate/pagination.options.interface';
import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { SpotService } from './spot.service';
import { SpotInput } from './spot.interface';
import { Response } from '../common/response.interface';
import { PubSub } from 'graphql-subscriptions';
import { Inject } from '@nestjs/common';
import { asyncFilter } from '../common/async.filter';
import * as wkx from 'wkx';

@Resolver('Spot')
export class SpotResolver {
  constructor(
    private readonly spotService: SpotService,
    @Inject('PubSub') private readonly pubSub: PubSub
  ) { }

  @Query()
  async getSpot(
    @Args('id') id: number): Promise<Response> {
    const data = await this.spotService.get(id);
    return { code: 200, data };
  }

  @Query()
  async getAllSpots(@Args('limit') limit: number, @Args('offset') offset: number): Promise<Response> {
    const options: PaginationOptionsInterface = { limit, offset };
    const data = await this.spotService.getAll(options);
    return { code: 200, data };
  }

  @Mutation()
  async createSpot(@Args('createSpotInput') createSpotInput: SpotInput): Promise<Response> {
    createSpotInput.geo_ponto = wkx.Geometry.parse(`POINT(${createSpotInput.latitude} ${createSpotInput.longitude})`).toGeoJSON();
    const data = await this.spotService.create(createSpotInput); 
    return { code: 200, message: 'Spot criado com sucesso', data }; 
  }

  @Mutation()
  async updateSpot(@Args('updateSpotInput') updateSpotInput: SpotInput): Promise<Response> {
    await this.spotService.update(updateSpotInput);
    return { code: 200, message: 'Spot atualizado com sucesso' };
  }

  @Mutation()
  async deleteSpot(@Args('id') id: number) {
    await this.spotService.delete(id);
    return { code: 200, message: 'Spot deletado com sucesso' };
  }

  @Mutation('playAudio')
  async playAudioMut(@Args('id_spot') id_spot: number) {
    this.pubSub.publish('playAudio', { playAudio: { code: 200, message: "Playing", path: "/audio" }, id_spot });
    return { code: 200, message: 'Play' };
  }

  @Mutation('changeColor')
  async changeColorMut(
    @Args('id_spot') id_spot: number,
    @Args('color') color: string
  ) {
    this.pubSub.publish('changeColor', { changeColor: { code: 200, message: `Change color to ${color}`, color }, id_spot });
    return { code: 200, message: `Changing color to ${color}` };
  }

  @Subscription('playAudio')
  playAudioSub(@Args('id_spot') id_spot: number) {
    return asyncFilter(
      this.pubSub.asyncIterator('playAudio'),
      (payload: any) => {
        return payload.id_spot === id_spot;
      }
    );
  }

  @Subscription('changeColor')
  changeColorSub(@Args('id_spot') id_spot: number) {
    return asyncFilter(
      this.pubSub.asyncIterator('changeColor'),
      (payload: any) => {
        return payload.id_spot === id_spot;
      }
    );
  }
}
