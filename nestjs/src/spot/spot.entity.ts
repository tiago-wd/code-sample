import moment from 'moment';
import { Column, Entity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, 
  ManyToMany, ManyToOne, OneToMany, JoinColumn, Index } from 'typeorm';
import { Usuario } from './../usuario/usuario.entity';
import { Conteudo } from './../conteudo/conteudo.entity';
import { ConfiguracaoSpot } from '../configuracao_spot/configuracao_spot.entity';
import { Geography } from 'geojson';

@Entity('spots')
export class Spot {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nome: string;

  @Column()
  descricao: string;

  @Column({
    nullable: true
  })
  imagem_perfil: string;

  @Column({
    nullable: true
  })
  imagem_capa: string;

  @Column('geography', {
    spatialFeatureType: 'Point',
    srid: 4326,
    nullable: true
  })
  @Index({ spatial: true })
  geo_ponto: Geography;

  @ManyToOne(type => Usuario, usuario => usuario.spotsOwner, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  @JoinColumn({ name: 'id_usuario' })
  usuario: Usuario;

  @ManyToMany(type => Usuario, usuario => usuario.spots, {
    onDelete: 'CASCADE',
  })
  usuarios: Usuario[];

  @OneToMany(type => Conteudo, conteudo => conteudo.spot)
  conteudos: Conteudo[];

  @CreateDateColumn({
    transformer: {
      from: (date: Date) => {
        return moment(date).format('YYYY-MM-DD HH:mm:ss');
      },
      to: () => {
        return new Date();
      },
    },
  })
  created_at: string;

  @UpdateDateColumn({
    transformer: {
      from: (date: Date) => {
        return moment(date).format('YYYY-MM-DD HH:mm:ss');
      },
      to: () => {
        return new Date();
      },
    },
  })
  updated_at: string;

  @OneToMany(type => ConfiguracaoSpot, configuracao_spot => configuracao_spot.spot)
  configuracoes_spots: ConfiguracaoSpot[];
}
