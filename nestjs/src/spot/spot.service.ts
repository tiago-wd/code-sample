import { Configuracao } from './../configuracao/configuracao.entity';
import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UsuarioService } from './../usuario/usuario.service';
import { Spot } from './spot.entity';
import { PaginationOptionsInterface } from '../common/paginate/pagination.options.interface';
import { Pagination } from '../common/paginate/pagination';
import { SpotInput } from './spot.interface';
import { ConfiguracaoSpotService } from '../configuracao_spot/configuracao_spot.service';

@Injectable()
export class SpotService {
  constructor(
    @InjectRepository(Spot) private readonly spotRepository: Repository<Spot>,
    @Inject(ConfiguracaoSpotService) private readonly configuracaoSpotService,
    @Inject(UsuarioService) private readonly usuarioService: UsuarioService,
  ) { }

  async get(id: number): Promise<Spot> {
    const spot = await this.spotRepository.findOne(id, {
      relations: [
        'usuario',
        'usuarios',
        'conteudos',
        'configuracoes_spots',
        'configuracoes_spots.configuracao'
      ]
    });
    const configuracoes = this.getConfiguracoes(spot);
    return { ...spot, ...configuracoes };
  }

  async getAll(options: PaginationOptionsInterface): Promise<Pagination<Spot>> {
    const [spots, total] = await this.spotRepository.findAndCount(
      {
        relations: ['usuario', 'usuarios', 'conteudos', 'configuracoes_spots', 'configuracoes_spots.configuracao'],
        take: options.limit,
        skip: options.offset,
        order: { created_at: "DESC" }
      }
    );

    const list: any[] = [];
    spots.map((spot) => {
      const configuracoes = this.getConfiguracoes(spot);
      list.push({ ...spot, ...configuracoes });
    })

    return await new Pagination<Spot>({ list, total });
  }

  async create(createSpotInput: SpotInput): Promise<Spot> {
    const usuario = await this.usuarioService.get(createSpotInput.id_usuario);
    const spot = await this.spotRepository.save(this.spotRepository.create({ ...createSpotInput, usuario }));

    if (createSpotInput.configuracoes) {
      await createSpotInput.configuracoes.map((configuracoes) => {
        this.configuracaoSpotService.create({ ...configuracoes, id_spot: spot.id });
      });
    }

    return await this.get(spot.id);
  }

  async update(updateSpotInput: SpotInput): Promise<void> {
    await this.spotRepository.save(await this.spotRepository.preload(updateSpotInput));
  }

  async delete(id: number): Promise<void> {
    const spot = await this.spotRepository.findOne(id);
    await this.spotRepository.remove(spot);
  }

  private getConfiguracoes(data: Spot): any {
    const configuracoes: any[] = [];
    data.configuracoes_spots.map((configuracoes_spots) => {
      configuracoes.push({ ...configuracoes_spots.configuracao, valor: configuracoes_spots.valor });
    });
    return { configuracoes };
  }

}
