import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GraphQLModule } from '@nestjs/graphql';
import { AppController } from './app.controller';
import { AuthService } from './auth/auth.service';
import { AuthGuard } from './auth/auth.guard';
import { PubSub } from 'graphql-subscriptions';
import { UsuarioModule } from './usuario/usuario.module';
import { SpotModule } from './spot/spot.module';
import { ConteudoModule } from './conteudo/conteudo.module';
import { ConfiguracaoModule } from './configuracao/configuracao.module';
import { ConfiguracaoSpotModule } from './configuracao_spot/configuracao_spot.module';
import { PointObject } from 'graphql-geojson';

@Module({
  controllers: [AppController],
  imports: [
    TypeOrmModule.forRoot(),
    GraphQLModule.forRoot({
      typePaths: ['./**/*.graphql'],
      playground: true,
      context: ({ req }) => ({ req }),
      installSubscriptionHandlers: true,
      resolvers: { Point: PointObject },
    }),
    UsuarioModule,
    SpotModule,
    ConteudoModule,
    ConfiguracaoModule,
    ConfiguracaoSpotModule,
  ],
  providers: [
    { provide: APP_GUARD, useClass: AuthGuard },
    { provide: PubSub, useValue: new PubSub },
    AuthService
  ]

})
export class AppModule { }
