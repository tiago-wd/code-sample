import { Usuario } from './../usuario/usuario.entity';
import jwt from 'jsonwebtoken';
import { Injectable } from '@nestjs/common';
import bcrypt from 'bcrypt';
import { AuthenticationError } from 'apollo-server-core';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class AuthService {

  constructor(
    @InjectRepository(Usuario) private readonly usuarioRepository: Repository<Usuario>,
  ) { }

  async createToken(usuario: Usuario) {
    const secretOrKey = 'secret';
    const expiresIn = '2 days';
    const token = jwt.sign({ usuario }, secretOrKey, { expiresIn });

    return { expiresIn, token };
  }

  public async isValidPassword(usuario: Usuario, password: string): Promise<boolean> {
    return await bcrypt.compare(password, usuario.password);
  }

  async validateUser(token: string, operationName: string) {
    if (['login', 'createUsuario', '/public/:file'].some(item => item === operationName)) {
      return true;
    }

    if (!token) {
      throw new AuthenticationError('Unauthenticated');
    }
    if (token.slice(0, 6) === 'Bearer') {
      token = token.slice(7);
    } else {
      throw new AuthenticationError('Invalid token');
    }

    try {
      return jwt.verify(token, 'secret');
    } catch (error) {
      if (error instanceof jwt.JsonWebTokenError) {
        throw new AuthenticationError('The authorization code is incorrect');
      }
      if (error instanceof jwt.TokenExpiredError) {
        throw new AuthenticationError('The authorization code has expired');
      }
    }
  }

}
