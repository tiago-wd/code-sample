import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { ConfiguracaoService } from './configuracao.service';
import { ConfiguracaoInput } from './configuracao.interface';
import { Response } from '../common/response.interface';
import { Inject } from '@nestjs/common';

@Resolver('Configuracao')
export class ConfiguracaoResolver {
  constructor(@Inject('ConfiguracaoService') private readonly configuracaoService: ConfiguracaoService) { }

  @Query()
  async getConfiguracao(@Args('id') id: number): Promise<Response> {
    const data = await this.configuracaoService.get(id);
    return { code: 200, data };
  }

  @Query()
  async getAllConfiguracoes(): Promise<Response> {
    const data = await this.configuracaoService.getAll();
    return { code: 200, data };
  }

  @Mutation()
  async createConfiguracao(@Args('createConfiguracaoInput') createConfiguracaoInput: ConfiguracaoInput): Promise<Response> {
    const data = await this.configuracaoService.create(createConfiguracaoInput);
    return { code: 200, message: 'Configuração criada com sucesso', data };
  }

  @Mutation()
  async updateConfiguracao(@Args('updateConfiguracaoInput') updateConfiguracaoInput: ConfiguracaoInput): Promise<Response> {
    await this.configuracaoService.update(updateConfiguracaoInput);
    return { code: 200, message: 'Configuração atualizada com sucesso' };
  }

  @Mutation()
  async deleteConfiguracao(@Args('id') id: number) {
    await this.configuracaoService.delete(id);
    return { code: 200, message: 'Configuração excluída com sucesso' };
  }
}
