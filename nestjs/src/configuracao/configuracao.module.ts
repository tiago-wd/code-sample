import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Configuracao } from './configuracao.entity';
import { ConfiguracaoResolver } from './configuracao.resolver';
import { ConfiguracaoService } from './configuracao.service';


@Module({
  imports: [
    TypeOrmModule.forFeature([Configuracao])
  ],
  providers: [Configuracao, ConfiguracaoService, ConfiguracaoResolver],
  exports: [ConfiguracaoService]
})

export class ConfiguracaoModule { }