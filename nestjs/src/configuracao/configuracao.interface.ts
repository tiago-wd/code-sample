export interface ConfiguracaoInput {
    id?: number;
    nome?: string;
    tipo?: string;
    descritivo_pai?: string;
    tamanho?: string;
    opcao_padrao?: string;
  }