import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Configuracao } from './configuracao.entity';
import { ConfiguracaoInput } from './configuracao.interface';

@Injectable()
export class ConfiguracaoService {
  constructor(@InjectRepository(Configuracao) private readonly configuracaoRepository: Repository<Configuracao>) { }

  async get(id: number): Promise<Configuracao> {
    return await this.configuracaoRepository.findOne(id);
  }

  async getAll(): Promise<Configuracao[]> {
    return await this.configuracaoRepository.find();
  }

  async create(createConfiguracaoInput: ConfiguracaoInput): Promise<Configuracao> {
    return await this.configuracaoRepository.save(this.configuracaoRepository.create(createConfiguracaoInput));
  }

  async update(updateConfiguracaoInput: ConfiguracaoInput): Promise<void> {
    await this.configuracaoRepository.save(await this.configuracaoRepository.preload(updateConfiguracaoInput));
  }

  async delete(id: number): Promise<void> {
    const spot = await this.configuracaoRepository.findOne(id);
    await this.configuracaoRepository.remove(spot);
  }
}
