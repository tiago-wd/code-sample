import moment from 'moment';
import { Column, Entity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToMany } from 'typeorm';
import { ConfiguracaoSpot } from '../configuracao_spot/configuracao_spot.entity';

@Entity('configuracoes')
export class Configuracao {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nome: string;

  @Column()
  tipo: string;

  @Column({
    nullable: true
  })
  descritivo_pai: string;

  @Column({
    nullable: true
  })
  tamanho: string;

  @Column()
  opcao_padrao: string;

  @CreateDateColumn({
    transformer: {
      from: (date: Date) => {
        return moment(date).format('YYYY-MM-DD HH:mm:ss');
      },
      to: () => {
        return new Date();
      },
    },
  })
  created_at: string;

  @UpdateDateColumn({
    transformer: {
      from: (date: Date) => {
        return moment(date).format('YYYY-MM-DD HH:mm:ss');
      },
      to: () => {
        return new Date();
      },
    },
  })
  updated_at: string;

  @OneToMany(type => ConfiguracaoSpot, configuracao_spot => configuracao_spot.configuracao)
  configuracoes_spots: ConfiguracaoSpot[];
}
