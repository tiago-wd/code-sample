import { AuthService } from './../auth/auth.service';
import { HttpException, Injectable, Inject } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Usuario } from './usuario.entity';
import { CreateUsuarioInput, SincronizarSpotInput } from './usuario.interface';
import { LoginInput } from '../auth/auth.interface';
import { Spot } from '../spot/spot.entity';

@Injectable()
export class UsuarioService {
  constructor(
    @InjectRepository(Usuario) private readonly usuarioRepository: Repository<Usuario>,
    @InjectRepository(Spot) private readonly spotRepository: Repository<Spot>,
    @Inject(AuthService) private readonly authService: AuthService,
  ) { }

  async get(id: number): Promise<Usuario> {
    return await this.usuarioRepository.findOne(id);
  }

  async create(createUsuarioInput: CreateUsuarioInput): Promise<Usuario> {
    if (createUsuarioInput.email && await this.usuarioRepository.findOne({ where: { email: createUsuarioInput.email } })) {
      throw new RpcException({ code: 409, message: 'Email já cadastrado.' });
    }
    return await this.usuarioRepository.save(this.usuarioRepository.create(createUsuarioInput));
  }

  async update(id: number, updateUsuarioInput: CreateUsuarioInput): Promise<void> {

    const usuario = await this.usuarioRepository.findOne(id);
    if (updateUsuarioInput.email && updateUsuarioInput.email !== usuario.email) {
      if (await this.usuarioRepository.findOne({ where: { email: updateUsuarioInput.email } })) {
        throw new HttpException('Email ja cadastrado', 409);
      }
    }

    await this.usuarioRepository.save(await this.usuarioRepository.preload({ ...updateUsuarioInput, id }));

  }

  async delete(id: number): Promise<void> {
    const usuario = await this.usuarioRepository.findOne(id);
    await this.usuarioRepository.remove(usuario);
  }

  async login(auth: LoginInput) {
    const { email, password } = auth;
    if (!email) {
      throw new HttpException('Email é obrigatório', 422);
    }
    if (!password) {
      throw new HttpException('Senha é obrigatória', 422);
    }

    const usuario = await this.usuarioRepository.findOne( {email}, { relations: ['spotsOwner'] } );

    if (!usuario) {
      throw new HttpException('Usuário não encontrado', 401);
    }
    if (!(await this.authService.isValidPassword(usuario, password))) {
      throw new HttpException('Senha inválda', 401);
    }
    const token = await this.authService.createToken(usuario);
    return { ...token, usuario };
  } 

  async connectSpot(sincronizarSpotInput: SincronizarSpotInput) {
    const usuario = await this.usuarioRepository.findOne({ id: sincronizarSpotInput.id_usuario });
    usuario.spots = [await this.spotRepository.findOne({ id: sincronizarSpotInput.id_spot })];
    return await this.usuarioRepository.save(usuario);
  }

  async disconnectSpot(sincronizarSpotInput: SincronizarSpotInput) {
    await this.usuarioRepository.createQueryBuilder('usuarios')
      .relation(Usuario, 'spots').of(sincronizarSpotInput.id_usuario)
      .remove(sincronizarSpotInput.id_spot);
  }

}
