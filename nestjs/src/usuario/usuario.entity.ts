import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
  OneToMany,
  BeforeInsert,
  BeforeUpdate
} from 'typeorm';
import { Spot } from '../spot/spot.entity';
import moment from 'moment';
import bcrypt from 'bcrypt';

@Entity('usuarios')
export class Usuario {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nome: string;

  @Column({
    unique: true,
  })
  email: string;

  @Column()
  password: string;

  @BeforeInsert()
  @BeforeUpdate()
  async hashPassword() {
    this.password = await bcrypt.hash(this.password, 10);
  }

  @OneToMany(type => Spot, spot => spot.usuario)
  spotsOwner: Spot[];

  @ManyToMany(type => Spot, spot => spot.usuarios, {
    onDelete: 'CASCADE',
  })
  @JoinTable({
    name: 'usuarios_spots',
    joinColumn: {
      name: 'id_usuario',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'id_spot',
      referencedColumnName: 'id',
    },
  })
  spots: Spot[];

  @CreateDateColumn({
    transformer: {
      from: (date: Date) => {
        return moment(date).format('YYYY-MM-DD HH:mm:ss');
      },
      to: () => {
        return new Date();
      },
    },
  })
  created_at: string;

  @UpdateDateColumn({
    transformer: {
      from: (date: Date) => {
        return moment(date).format('YYYY-MM-DD HH:mm:ss');
      },
      to: () => {
        return new Date();
      },
    },
  })
  updated_at: string;
}
