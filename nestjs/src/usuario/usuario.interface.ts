export interface CreateUsuarioInput {
  id?: number;
  nome: string;
  email: string;
  password: string;
}

export interface SincronizarSpotInput {
  id_usuario: number;
  id_spot: number;
}