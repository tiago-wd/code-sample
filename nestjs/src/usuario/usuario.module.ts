import { Spot } from './../spot/spot.entity';
import { AuthService } from './../auth/auth.service';
import { UsuarioResolver } from './usuario.resolver';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Usuario } from './usuario.entity';
import { UsuarioService } from './usuario.service';
import { PubSub } from 'graphql-subscriptions';

@Module({
  imports: [
    TypeOrmModule.forFeature([Usuario, Spot]),
  ],
  providers: [UsuarioService, UsuarioResolver, AuthService, PubSub],
  exports: [UsuarioService]
})

export class UsuarioModule { }
