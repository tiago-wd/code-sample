import { Args, Mutation, Resolver, Subscription } from '@nestjs/graphql';
import { UsuarioService } from './usuario.service';
import { LoginInput } from '../auth/auth.interface';
import { CreateUsuarioInput, SincronizarSpotInput } from './usuario.interface';
import { Response } from '../common/response.interface';
import { Inject } from '@nestjs/common';
import { PubSub } from 'graphql-subscriptions';
import { asyncFilter } from '../common/async.filter';

@Resolver('Usuario')
export class UsuarioResolver {
  constructor(
    private readonly usuarioService: UsuarioService,
    @Inject('PubSub') private readonly pubSub: PubSub
  ) { }

  @Mutation()
  async createUsuario(
    @Args('nome') nome: string,
    @Args('email') email: string,
    @Args('password') password: string,
  ) {
    const usuario: CreateUsuarioInput = { nome, email, password };
    return await this.usuarioService.create(usuario);
  }

  @Mutation()
  async updateUsuario(
    @Args('id') id: number,
    @Args('nome') nome: string,
    @Args('email') email: string,
    @Args('password') password: string,
  ) {
    const usuario: CreateUsuarioInput = { nome, email, password };
    await this.usuarioService.update(id, usuario);
    return { code: 200, message: 'Usuário atualizado com sucesso' };
  }

  @Mutation()
  async deleteUsuario(
    @Args('id') id: number,
  ) {
    await this.usuarioService.delete(id);
    return { code: 200, message: 'Usuário deletado com sucesso' };
  }

  @Mutation()
  async login(
    @Args('email') email: string,
    @Args('password') password: string,
  ) {
    const auth: LoginInput = { email, password };
    return await this.usuarioService.login(auth);
  }

  @Mutation()
  async connectSpot(@Args('sincronizarSpotInput') sincronizarSpotInput: SincronizarSpotInput): Promise<Response> {
    const usuarioSpot = await this.usuarioService.connectSpot(sincronizarSpotInput);
    this.pubSub.publish('conectado', { conectado: usuarioSpot.spots[0], id_spot: usuarioSpot.spots[0].id }).catch(console.log);
    return { code: 200, message: 'Usuário conectado ao Spot' };
  }

  @Mutation()
  async disconnectSpot(@Args('sincronizarSpotInput') sincronizarSpotInput: SincronizarSpotInput): Promise<Response> {
    await this.usuarioService.disconnectSpot(sincronizarSpotInput);
    return { code: 200, message: 'Usuário desconectado ao Spot' };
  }

  @Subscription('conectado')
  conectado(@Args('id_spot') id_spot: number) {
    return asyncFilter(
      this.pubSub.asyncIterator('conectado'),
      (payload: any) => {
        return payload.id_spot === id_spot;
      }
    );
  }
}

