import { Injectable } from '@nestjs/common';
import { ConfiguracaoSpotInput } from './configuracao_spot.interface';
import { InjectRepository } from '@nestjs/typeorm';
import { ConfiguracaoSpot } from './configuracao_spot.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ConfiguracaoSpotService {
  constructor(@InjectRepository(ConfiguracaoSpot) private readonly configuracaoSpotRepository: Repository<ConfiguracaoSpot>) { }

  async create(createconfiguracaoSpotInput: ConfiguracaoSpotInput): Promise<ConfiguracaoSpot> {
    return await this.configuracaoSpotRepository.save(this.configuracaoSpotRepository.create(createconfiguracaoSpotInput));
  }

}
