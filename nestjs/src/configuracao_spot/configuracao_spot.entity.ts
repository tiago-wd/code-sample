import moment from 'moment';
import { Column, Entity, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn, PrimaryColumn } from 'typeorm';
import { Spot } from '../spot/spot.entity';
import { Configuracao } from '../configuracao/configuracao.entity';

@Entity('configuracoes_spots')
export class ConfiguracaoSpot {
  @PrimaryColumn()
  id_configuracao: number;

  @PrimaryColumn()
  id_spot: number;

  @Column()
  valor: string;

  @CreateDateColumn({
    transformer: {
      from: (date: Date) => {
        return moment(date).format('YYYY-MM-DD HH:mm:ss');
      },
      to: () => {
        return new Date();
      },
    },
  })
  created_at: string;

  @UpdateDateColumn({
    transformer: {
      from: (date: Date) => {
        return moment(date).format('YYYY-MM-DD HH:mm:ss');
      },
      to: () => {
        return new Date();
      },
    },
  })
  updated_at: string;

  @ManyToOne(type => Configuracao, configuracao => configuracao.configuracoes_spots, {
    onDelete: 'CASCADE',
    nullable: true,
  })
  @JoinColumn({ name: 'id_configuracao' })
  configuracao: Configuracao;

  @ManyToOne(type => Spot, spot => spot.configuracoes_spots, {
    onDelete: 'CASCADE',
    nullable: true,
  })
  @JoinColumn({ name: 'id_spot' })
  spot: Spot;
}