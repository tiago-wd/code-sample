import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfiguracaoSpot } from './configuracao_spot.entity';
import { ConfiguracaoSpotService } from './configuracao_spot.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([ConfiguracaoSpot]),
    ConfiguracaoSpotModule
  ],
  providers: [ConfiguracaoSpotService],
  exports: [ConfiguracaoSpotService]
})

export class ConfiguracaoSpotModule { }
