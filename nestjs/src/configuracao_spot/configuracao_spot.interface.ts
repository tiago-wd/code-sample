export interface ConfiguracaoSpotInput {
  id_configuracao?: number;
  id_spot?: number;
  valor?: string;
}