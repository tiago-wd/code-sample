import { FileInterceptor } from '@nestjs/platform-express';
import { CreateConteudoInput } from './conteudo/conteudo.interface';
import { ConteudoService } from './conteudo/conteudo.service';
import { Get, Controller, Res, Post, UseInterceptors, UploadedFile, Body, Inject, Param } from '@nestjs/common';
import { diskStorage } from 'multer';

@Controller()
export class AppController {

  constructor(@Inject(ConteudoService) private readonly conteudoService: ConteudoService) { }

  @Get('public/:file')
  public(@Res() res, @Param('file') file) {
    return res.sendFile(file, {
      root: 'public'
    });
  }

  @Post('file')
  @UseInterceptors(FileInterceptor('file', {
    storage: diskStorage({
      destination: 'public',
      filename: (req, file, cb) => {
        const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
        return cb(null, `${randomName}${(file.originalname)}`)
      }
    })
  }
  ))
  async file(@UploadedFile() file, @Body() body: CreateConteudoInput) {
    const nome = body.nome ? body.nome : file.filename;
    const tipo = file.mimetype;
    const caminho = file.path;
    const id_spot = body.id_spot;
    const createConteudoInput: CreateConteudoInput = { nome, tipo, caminho, id_spot }
    const data = await this.conteudoService.create(createConteudoInput);
    return { code: 200, data };
  }
}