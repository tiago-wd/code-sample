export interface CreateConteudoInput {
  id?: number;
  nome: string;
  tipo: string;
  caminho: string;
  id_spot: number;
}
