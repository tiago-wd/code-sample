import { Args, Resolver, Query } from '@nestjs/graphql';
import { ConteudoService } from './conteudo.service';
import { Response } from '../common/response.interface';

@Resolver('Conteudo')
export class ConteudoResolver {
  constructor(private readonly conteudoService: ConteudoService) { }

  @Query()
  async getConteudo(@Args('id') id: number): Promise<Response> {
    const data = await this.conteudoService.get(id);
    return { code: 200, data };
  }

  @Query()
  async getAllConteudos(): Promise<Response> {
    const data = await this.conteudoService.getAll();
    return { code: 200, data };
  }

}
