import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Conteudo } from './conteudo.entity';
import { ConteudoResolver } from './conteudo.resolver';
import { ConteudoService } from './conteudo.service';
import { SpotModule } from '../spot/spot.module';


@Module({
  imports: [
    TypeOrmModule.forFeature([Conteudo]),
    SpotModule
  ],
  providers: [ConteudoService, ConteudoResolver],
  exports: [ConteudoService]
})
export class ConteudoModule { }
