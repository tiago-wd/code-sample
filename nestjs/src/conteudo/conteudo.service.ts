import { SpotService } from './../spot/spot.service';
import { AuthService } from './../auth/auth.service';
import { Injectable, Inject, HttpException, Body } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Conteudo } from './conteudo.entity';
import { Repository } from 'typeorm';
import { CreateConteudoInput } from './conteudo.interface';

@Injectable()
export class ConteudoService {

  constructor(
    @InjectRepository(Conteudo) private readonly conteudoRepository: Repository<Conteudo>,
    @Inject('SpotService') private readonly spotService: SpotService
  ) { }

  async get(id: number): Promise<Conteudo> {
    return await this.conteudoRepository.findOne(id, { relations: ['spot', 'spot.usuario', 'spot.usuarios'] });
  }

  async getAll(): Promise<Conteudo[]> {
    return await this.conteudoRepository.find({ relations: ['spot', 'spot.usuario', 'spot.usuarios'] });
  }

  async create(createConteudoInput: CreateConteudoInput): Promise<Conteudo> {
    const spot = await this.spotService.get(createConteudoInput.id_spot);
    return await this.conteudoRepository.save(this.conteudoRepository.create({ ...createConteudoInput, spot }));
  }

}
