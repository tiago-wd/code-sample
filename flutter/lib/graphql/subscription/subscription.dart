import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:rodofret/graphql/subscription/message.dart';
import 'package:rodofret/graphql/subscription/ws_server.dart';
import 'package:rxdart/rxdart.dart';
import 'package:uuid/uuid.dart';

SocketClient socketClient;

final Uuid _uuid = Uuid();

class SocketClient {
  static Future<SocketClient> connect(
    final String endPoint, {
    final List<String> protocols = const [
      'graphql-ws',
    ],
    final Map<String, String> headers = const {
      'content-type': 'application/json',
    },
  }) async {
    return SocketClient(
      GraphQLSocket(
        await WebSocket.connect(
          endPoint,
          protocols: protocols,
          headers: headers,
        ),
      ),
    );
  }

  final GraphQLSocket _socket;

  SocketClient(this._socket) {
    print('connect init');
  }

  Stream<dynamic> subscribe(final payload) {
    final String id = _uuid.v4();
    final StreamController<SubscriptionData> response = StreamController<SubscriptionData>();

    final Stream<SubscriptionComplete> complete =
        _socket.subscriptionComplete.where((message) => message.id == id).take(1);

    final Stream<SubscriptionData> data =
        _socket.subscriptionData.where((message) => message.id == id).takeWhile((_) => !response.isClosed);

    final Stream<SubscriptionError> error =
        _socket.subscriptionError.where((message) => message.id == id).takeWhile((_) => !response.isClosed);

    complete.listen((_) => response.close());
    data.listen((message) => response.add(message));
    error.listen((message) => response.addError(message));

    response.onListen = () => _socket.write({'id': id, 'type': 'start', 'payload': payload});
    response.onCancel = () => _socket.write({'id': id});

    return response.stream;
  }
}

// import 'dart:async';
// import 'dart:convert';
// import 'dart:io';

// class WebsocketService {
//   // final StreamController<dynamic> _subject = StreamController<dynamic>.broadcast();
//   Future<WebSocket> webSocket;

//   WebsocketService() {
//     connect();
//   }

//   void connect() {
//     webSocket = WebSocket.connect('ws://192.168.1.103:3000/graphql', protocols: ['graphql-ws']);
//   }

//   // void onConnected() {
//   //   webSocket.onMessage.listen((e) {
//   //     onMessage(e.data);
//   //   });
//   // }

//   // void onDisconnected() {
//   //   print("Disconnected, trying again in 3s");
//   //   new Timer(new Duration(seconds: 3), () {
//   //     connect();
//   //   });
//   // }

//   void send(data) {
//     webSocket.then((ws) => ws.add(json.encode(data, toEncodable: (m) => m.toJson())));
//   }

//   void subscribe(Function function) async {
//     // _subject.stream.listen(print);
//     // webSocket.map((message) => json.decode(message)).listen((message) {
//     //   print(message);
//     // });
//     webSocket.then((ws) {
//       ws.listen(function);
//     });
//   }

//   void dispose() {
//     webSocket.then((ws) {
//       ws.close();
//     });
//   }
// }
