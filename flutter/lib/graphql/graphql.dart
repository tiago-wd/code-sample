import 'package:rodofret/graphql/client.dart';
import 'package:rxdart/rxdart.dart';

Observable<dynamic> mutation(
  String body, {
  Map<String, dynamic> variables,
}) {
  client = getClient();
  dynamic result = client.query(query: body, variables: variables);
  return new Observable.fromFuture(result).flatMap((dynamic response) {
    return Observable.just(response.data['data']);
  });
}

Observable<dynamic> query(
  String body, {
  Map<String, dynamic> variables,
}) {
  client = getClient();
  dynamic result = client.query(query: body, variables: variables);
  return new Observable.fromFuture(result).flatMap((dynamic response) {
    return Observable.just(response.data['data']);
  });
}
