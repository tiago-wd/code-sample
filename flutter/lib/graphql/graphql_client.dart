import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';

class GraphqlClient {
  GraphqlClient({
    String endPoint = '',
    String apiToken,
  }) {
    assert(endPoint != null);

    this.endPoint = endPoint;
    this.apiToken = apiToken;
  }

  String _endpoint;
  String _apiToken;
  Dio dio = new Dio();

  set endPoint(String value) {
    _endpoint = value;
  }

  set apiToken(String value) {
    _apiToken = value;
  }

  String get endPoint => this._endpoint;

  String get apiToken => this._apiToken;

  Dio get dioInstance => this.dio;

  Map<String, String> get headers => {
        'Authorization': 'Bearer $apiToken',
        'Content-Type': 'application/json',
      };

  String _encodeBody(
    String query, {
    Map<String, dynamic> variables,
  }) {
    return json.encode({
      'query': query,
      'variables': variables,
    });
  }

  Future<dynamic> query({
    String query,
    Map<String, dynamic> variables,
  }) async {
    final String body = _encodeBody(
      query,
      variables: variables,
    );

    print(endPoint);
    print(body);
    try {
      final request = await dio.post(
        endPoint,
        data: body,
        options: Options(
          headers: headers,
        ),
      );

      return request;
    } on SocketException {
      throw 'Error';
    }
  }
}
