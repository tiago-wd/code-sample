import 'package:rodofret/graphql/graphql_client.dart';
import 'package:rodofret/env.dart';

GraphqlClient client = GraphqlClient(
  endPoint: env.getBaseUrlGraphql(),
);

setToken(String token) {
  client.apiToken = token;
}

getClient() {
  return client;
}
