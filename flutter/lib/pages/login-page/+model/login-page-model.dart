import 'package:redux/redux.dart';
import 'package:rodofret/app-state.dart';
import 'package:rodofret/pages/login-page/+state/login-actions.dart';

class LoginPageModel {
  final Function(dynamic formText) loginAction;
  final Function() registerPage;

  LoginPageModel({
    this.loginAction,
    this.registerPage,
  });

  static LoginPageModel fromStore(Store<AppState> store) => new LoginPageModel(
        loginAction: (formText) => store.dispatch(new LoginAction(formText)),
        registerPage: () => store.dispatch(new ToRegisterPage()),
      );
}
