import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:rodofret/app-state.dart';
import 'package:rodofret/pages/login-page/+model/login-page-model.dart';
import 'package:rodofret/pages/login-page/components/login-form.dart';
import 'package:rodofret/pages/login-page/components/login-buttons.dart';
import 'package:rodofret/widget/login-background.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with SingleTickerProviderStateMixin {
  final formLogin = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  AnimationController controller;
  Animation<double> animation;
  var deviceSize;

  @override
  initState() {
    super.initState();
    controller = new AnimationController(vsync: this, duration: new Duration(milliseconds: 1500));
    animation =
        new Tween(begin: 0.0, end: 1.0).animate(new CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn));
    animation.addListener(() => this.setState(() {}));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;

    return new StoreConnector<AppState, dynamic>(
        converter: (store) => LoginPageModel.fromStore(store),
        builder: (context, loginPageModel) {
          return Scaffold(
              backgroundColor: Color(0xffeeeeee),
              body: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  LoginBackground(),
                  Center(
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: 100.0,
                          ),
                          loginCard(loginPageModel.loginAction, loginPageModel.registerPage),
                        ],
                      ),
                    ),
                  )
                ],
              ));
        });
  }

  Widget loginCard(loginAction, registerPage) {
    return SizedBox(
      height: deviceSize.height / 2 - 20,
      width: deviceSize.width * 0.85,
      child: new Card(
          color: Colors.white,
          elevation: 2.0,
          child: Column(children: <Widget>[
            form(formLogin, emailController, passwordController),
            loginButton(loginAction, formLogin, emailController, passwordController),
            register(registerPage)
          ])),
    );
  }
}
