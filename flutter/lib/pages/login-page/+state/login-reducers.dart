import 'package:redux/redux.dart';
import 'package:rodofret/pages/login-page/+state/login-actions.dart';
import 'package:rodofret/pages/login-page/+state/login-state.dart';

final loginReducer = combineReducers<LoginState>([
  TypedReducer<LoginState, LoginAction>(_loginAction),
  TypedReducer<LoginState, ForgotPasswordAction>(_forgotPasswordAction),
  TypedReducer<LoginState, ToRegisterPage>(_registerPage),
  TypedReducer<LoginState, LoginError>(_loginError),
  TypedReducer<LoginState, LoginSuccess>(_loginSuccess),
]);

LoginState _loginAction(LoginState state, LoginAction action) => state;

LoginState _forgotPasswordAction(LoginState state, ForgotPasswordAction action) => state;

LoginState _loginError(LoginState state, LoginError action) => state;

LoginState _registerPage(LoginState state, ToRegisterPage action) => state;

LoginState _loginSuccess(LoginState state, LoginSuccess action) {
  return state.copyWith(loadingStatus: false, token: action.login['token'], id: action.login['usuario']['id']);
}
