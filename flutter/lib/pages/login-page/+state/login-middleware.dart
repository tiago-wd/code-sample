import 'package:redux/redux.dart';
import 'package:rodofret/graphql/client.dart';
import 'package:rodofret/graphql/graphql.dart';
import 'package:rodofret/keys.dart';
import 'package:rodofret/pages/login-page/+state/login-actions.dart';

class LoginMiddleware extends MiddlewareClass {
  String loginMutation = """
      mutation login(\$email: String!, \$password: String!){
        login(email: \$email, password: \$password) {
          token
          expiresIn
          user {
            id
            name
            email
          }
        }
      }
    """
      .replaceAll('\n', ' ');

  @override
  void call(Store store, action, NextDispatcher next) {
    if (action is LoginAction) {
      Keys.navKey.currentState.pushNamed('freight-list-page');
      // mutation(loginMutation, variables: {'email': action.userLogin['email'], 'password': action.userLogin['password']})
      //     .map((dynamic data) {
      //   if (data.containsKey('errors')) {
      //     store.dispatch(new LoginError());
      //   } else {
      //     setToken(data['login']['token']);
      //     store.dispatch(new LoginSuccess(data['login']));
      //   }
      // }).listen(
      //   print,
      //   onDone: () => print('done'),
      // );
    }

    if (action is ForgotPasswordAction) {
      print(action);
    }

    if (action is LoginError) {
      print(action);
    }

    if (action is ToRegisterPage) {
      Keys.navKey.currentState.pushNamed("register-page");
    }

    if (action is LoginSuccess) {
      Keys.navKey.currentState.pushNamed("home-page");
    }
    next(action);
  }
}
