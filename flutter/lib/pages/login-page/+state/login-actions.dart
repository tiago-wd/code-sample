class LoginAction {
  final userLogin;
  LoginAction(this.userLogin);
}

class LoginError {}

class ForgotPasswordAction {}

class ToRegisterPage {}

class LoginSuccess {
  final login;
  LoginSuccess(this.login);
}
