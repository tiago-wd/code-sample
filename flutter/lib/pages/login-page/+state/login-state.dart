import 'package:meta/meta.dart';

@immutable
class LoginState {
  final loadingStatus;
  final String email;
  final String token;
  final int id;

  LoginState({
    @required this.loadingStatus,
    @required this.email,
    @required this.token,
    @required this.id,
  });

  factory LoginState.initial() {
    return new LoginState(
      loadingStatus: false,
      email: "",
      token: "",
      id: 0,
    );
  }

  LoginState copyWith({
    loadingStatus,
    String email,
    String token,
    int id,
  }) {
    return new LoginState(
      loadingStatus: loadingStatus ?? this.loadingStatus,
      email: email ?? this.email,
      token: token ?? this.token,
      id: id ?? this.id,
    );
  }
}
