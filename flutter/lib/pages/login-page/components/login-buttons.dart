import 'package:flutter/material.dart';

final loginButton = (loginAction, formLogin, emailController, passwordController) => Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0),
      child: FlatButton(
        onPressed: () {
          loginAction({"email": 'emailController', "password": 'passwordController'});
          if (formLogin.currentState.validate()) {
            var formText = {"email": emailController.text, "password": passwordController.text};
            loginAction(formText);
          }
        },
        padding: EdgeInsets.all(12),
        child: Text('ENTRAR', style: TextStyle(color: Color.fromRGBO(57, 184, 91, 0.8), fontSize: 25)),
      ),
    );

final register = (registerPage) => FlatButton(
      child: Text(
        'Cadastre-se!',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () => registerPage(),
    );
