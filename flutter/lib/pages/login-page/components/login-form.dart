import 'package:flutter/material.dart';
import 'package:rodofret/style/style-form.dart';
import 'package:rodofret/style/decoration-form.dart';

final form = (formLogin, emailController, passwordController) => Form(
      key: formLogin,
      child: Padding(
        padding: const EdgeInsets.all(18.0),
        child: SingleChildScrollView(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              TextFormField(
                controller: emailController,
                keyboardType: TextInputType.emailAddress,
                autofocus: false,
                validator: (value) {
                  Pattern pattern =
                      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                  RegExp regex = new RegExp(pattern);
                  if (value.isEmpty) return 'Por favor digite um e-mail';
                  if (!regex.hasMatch(value)) return 'Entre com um e-mail válido';
                },
                style: inputStyle(),
                decoration: inputDecoration('Email'),
              ),
              new SizedBox(
                height: 10.0,
              ),
              TextFormField(
                controller: passwordController,
                autofocus: false,
                obscureText: true,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Por favor digite uma senha';
                  }
                },
                style: inputStyle(),
                decoration: inputDecoration('Password'),
              )
            ],
          ),
        ),
      ),
    );

final buildError = (hasError) => hasError
    ? Container(
        child: Text(
          'Senha Inválida',
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.red),
        ),
      )
    : Container();
