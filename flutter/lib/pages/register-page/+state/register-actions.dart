class RegisterAction {
  final newUser;
  RegisterAction(this.newUser);
}

class RegisterError {}

class ToLoginPage {}

class RegisterSuccess {
  final data;
  RegisterSuccess(this.data);
}
