import 'package:meta/meta.dart';

@immutable
class RegisterState {
  final userModel;

  RegisterState({
    @required this.userModel,
  });

  factory RegisterState.initial() {
    return new RegisterState(
      userModel: null,
    );
  }

  RegisterState copyWith({
    userModel,
  }) {
    return new RegisterState(
      userModel: userModel ?? this.userModel,
    );
  }
}
