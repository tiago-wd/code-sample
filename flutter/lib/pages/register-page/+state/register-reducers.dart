import 'package:redux/redux.dart';
import 'package:rodofret/pages/register-page/+state/register-actions.dart';
import 'package:rodofret/pages/register-page/+state/register-state.dart';

final registerReducer = combineReducers<RegisterState>([
  TypedReducer<RegisterState, RegisterAction>(_registerAction),
  TypedReducer<RegisterState, RegisterError>(_registerError),
  TypedReducer<RegisterState, RegisterSuccess>(_registerSuccess),
  TypedReducer<RegisterState, ToLoginPage>(_loginPage),
]);

RegisterState _registerAction(RegisterState state, RegisterAction action) => state;

RegisterState _registerError(RegisterState state, RegisterError action) => state;

RegisterState _loginPage(RegisterState state, ToLoginPage action) => state;

RegisterState _registerSuccess(RegisterState state, RegisterSuccess action) => state;
