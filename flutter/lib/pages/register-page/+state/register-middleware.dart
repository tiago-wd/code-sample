import 'package:redux/redux.dart';
import 'package:rodofret/graphql/graphql.dart';
import 'package:rodofret/keys.dart';
import 'package:rodofret/pages/register-page/+state/register-actions.dart';

class RegisterMiddleware extends MiddlewareClass {
  String registerMutation = """
      mutation register(\$createUserInput: CreateUserInput){
        register(createUserInput: \$createUserInput) {
          code
          message
          data {
            id
            name
            email
          }
        }
      }
    """
      .replaceAll('\n', ' ');
  @override
  void call(Store store, action, NextDispatcher next) {
    if (action is RegisterAction) {
      mutation(registerMutation, variables: action.newUser).map((dynamic data) {
        if (data.containsKey('errors')) {
          store.dispatch(new RegisterError());
        } else {
          store.dispatch(new RegisterSuccess(data['createUsuario']));
        }
      }).listen(
        print,
        onDone: () => print('done'),
      );
    }

    if (action is RegisterSuccess) {
      Keys.navKey.currentState.pushNamed("login-page");
    }

    if (action is ToLoginPage) {
      Keys.navKey.currentState.pushNamed("login-page");
    }

    next(action);
  }
}
