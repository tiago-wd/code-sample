import 'package:flutter/material.dart';
import 'package:rodofret/style/style-form.dart';
import 'package:rodofret/style/decoration-form.dart';

List<String> roles = <String>['', 'Motorista', 'Usuário'];
String role = '';

final form = (formRegister, emailController, nameController, confirmPasswordController, passwordController) => Form(
    key: formRegister,
    child: Padding(
        padding: const EdgeInsets.all(18.0),
        child: SingleChildScrollView(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextFormField(
              controller: emailController,
              keyboardType: TextInputType.emailAddress,
              autofocus: false,
              validator: (value) {
                Pattern pattern =
                    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                RegExp regex = new RegExp(pattern);
                if (value.isEmpty) return 'Por favor digite um e-mail';
                if (!regex.hasMatch(value)) return 'Entre com um e-mail válido';
              },
              style: inputStyle(),
              decoration: inputDecoration('Email'),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 3.0),
            ),
            TextFormField(
              controller: nameController,
              keyboardType: TextInputType.text,
              autofocus: false,
              validator: (value) {
                if (value.isEmpty) return 'Por favor digite seu nome';
              },
              style: inputStyle(),
              decoration: inputDecoration('Nome'),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 3.0),
            ),
            TextFormField(
              controller: passwordController,
              keyboardType: TextInputType.text,
              autofocus: false,
              obscureText: true,
              validator: (value) {
                if (value.isEmpty) return 'Por favor digite uma senha';
              },
              style: inputStyle(),
              decoration: inputDecoration('Senha'),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 3.0),
            ),
            TextFormField(
              controller: confirmPasswordController,
              keyboardType: TextInputType.text,
              autofocus: false,
              obscureText: true,
              validator: (value) {
                if (value.isEmpty) return 'Por favor digite a confirmação de senha';
              },
              style: inputStyle(),
              decoration: inputDecoration('Confirmar senha'),
            ),
          ],
        ))));
