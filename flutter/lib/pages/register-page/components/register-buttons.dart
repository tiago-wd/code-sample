import 'package:flutter/material.dart';

final registerButton =
    (formRegister, emailController, passwordController, confirmPasswordController, nameController, registerAction) =>
        Padding(
          padding: EdgeInsets.symmetric(vertical: 20.0),
          child: FlatButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            onPressed: () {
              if (formRegister.currentState.validate()) {
                if (confirmPasswordController.text == passwordController.text) {
                  var formText = {
                    "createUserInput": {
                      "email": emailController.text,
                      "password": passwordController.text,
                      "name": nameController.text
                    }
                  };
                  registerAction(formText);
                }
              }
            },
            padding: EdgeInsets.all(12),
            child: Text('Criar Conta', style: TextStyle(color: Color.fromRGBO(57, 184, 91, 0.8), fontSize: 25)),
          ),
        );

final login = (loginPage) => FlatButton(
      child: Text(
        'Login',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () => loginPage(),
    );
