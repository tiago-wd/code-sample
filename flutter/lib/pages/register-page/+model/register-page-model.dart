import 'package:redux/redux.dart';
import 'package:rodofret/app-state.dart';
import 'package:rodofret/pages/register-page/+state/register-actions.dart';

class RegisterPageModel {
  final Function(dynamic newUser) registerAction;
  final Function() loginPage;

  RegisterPageModel({
    this.registerAction,
    this.loginPage,
  });

  static RegisterPageModel fromStore(Store<AppState> store) => new RegisterPageModel(
        registerAction: (newUser) => store.dispatch(new RegisterAction(newUser)),
        loginPage: () => store.dispatch(new ToLoginPage()),
      );
}
