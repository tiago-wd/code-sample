import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:rodofret/app-state.dart';
import 'package:rodofret/widget/login-background.dart';
import 'package:rodofret/pages/register-page/+model/register-page-model.dart';
import 'package:rodofret/pages/register-page/components/register-form.dart';
import 'package:rodofret/pages/register-page/components/register-buttons.dart';

class RegisterPage extends StatefulWidget {
  static String tag = 'register-page';
  @override
  _RegisterPageState createState() => new _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final formRegister = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final nameController = TextEditingController();
  final confirmPasswordController = TextEditingController();
  final passwordController = TextEditingController();

  AnimationController controller;
  Animation<double> animation;
  var deviceSize;

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;

    return new StoreConnector<AppState, dynamic>(
        converter: (store) => RegisterPageModel.fromStore(store),
        builder: (context, registerPageModel) {
          return Scaffold(
            backgroundColor: Color(0xffeeeeee),
            body: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                LoginBackground(),
                Center(
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 50.0,
                        ),
                        registerCard(registerPageModel.registerAction, registerPageModel.loginPage)
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  Widget registerCard(registerAction, loginPage) {
    return SizedBox(
      height: deviceSize.height / 1.4,
      width: deviceSize.width * 0.85,
      child: new Card(
          color: Colors.white,
          elevation: 2.0,
          child: Column(children: <Widget>[
            form(formRegister, emailController, nameController, confirmPasswordController, passwordController),
            registerButton(formRegister, emailController, passwordController, confirmPasswordController, nameController,
                registerAction),
            login(loginPage)
          ])),
    );
  }
}
