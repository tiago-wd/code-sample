import 'package:flutter/material.dart';
import 'package:rodofret/style/decoration-form.dart';

class FreightCreateForm extends StatefulWidget {
  const FreightCreateForm({this.nameController, this.descriptionController, this.formCreateFreight});
  final TextEditingController nameController;
  final TextEditingController descriptionController;
  final GlobalKey<FormState> formCreateFreight;
  _FreightCreateFormState createState() => _FreightCreateFormState();
}

class _FreightCreateFormState extends State<FreightCreateForm> {
  final form = (formCreateFreight, nameController, descriptionController) => Form(
      key: formCreateFreight,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            controller: nameController,
            keyboardType: TextInputType.text,
            style: TextStyle(color: Colors.black, fontSize: 18),
            autofocus: false,
            validator: (value) {
              if (value.isEmpty) return 'Por favor digite um título';
            },
            decoration: inputDecoration('Título do frete'),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
          ),
          TextFormField(
            controller: descriptionController,
            keyboardType: TextInputType.text,
            style: TextStyle(color: Colors.black, fontSize: 18),
            autofocus: false,
            validator: (value) {
              if (value.isEmpty) {
                return 'Por favor digite uma descrição';
              }
            },
            decoration: inputDecoration('Descrição'),
          ),
        ],
      ));

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              form(widget.formCreateFreight, widget.nameController, widget.descriptionController),
            ],
          ),
        ),
      ],
    );
  }
}
