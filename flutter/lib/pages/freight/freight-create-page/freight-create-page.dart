import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:rodofret/app-state.dart';
import 'package:rodofret/pages/freight/freight-create-page/+model/freight-create-page-model.dart';
import 'package:rodofret/pages/freight/freight-create-page/freight-create-form/freight-create-form.dart';

class FreightCreatePage extends StatefulWidget {
  static String tag = 'freight-create-page';
  @override
  State<StatefulWidget> createState() {
    return FreightCreatePageState();
  }
}

class FreightCreatePageState extends State<FreightCreatePage> {
  static String tag = 'freight-create-page';
  final createForm = GlobalKey<FormState>();
  final nameController = TextEditingController();
  final descriptionController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, FreightCreatePageModel>(
        converter: (store) => FreightCreatePageModel.fromStore(store),
        builder: (context, freightCreatePageModel) {
          return Scaffold(
            body: FreightCreateForm(
                nameController: nameController,
                descriptionController: descriptionController,
                formCreateFreight: createForm),
            appBar: AppBar(title: Text('Criar frete'), actions: [
              FlatButton(
                child: Text('Salvar', style: TextStyle(color: Colors.white, fontSize: 18)),
                onPressed: () {
                  if (createForm.currentState.validate())
                    freightCreatePageModel
                        .freightCreate({'name': nameController.text, 'description': descriptionController.text});
                },
              )
            ]),
          );
        });
  }
}
