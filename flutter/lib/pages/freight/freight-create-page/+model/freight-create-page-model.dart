import 'package:redux/redux.dart';
import 'package:rodofret/app-state.dart';
import 'package:rodofret/pages/freight/freight-create-page/+state/freight-create-page-actions.dart';

class FreightCreatePageModel {
  final Function(dynamic freight) freightCreate;

  FreightCreatePageModel({
    this.freightCreate,
  });

  static FreightCreatePageModel fromStore(Store<AppState> store) => new FreightCreatePageModel(
        freightCreate: (freight) => store.dispatch(new FreightCreate(freight)),
      );
}
