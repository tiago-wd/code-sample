import 'package:redux/redux.dart';
import 'package:rodofret/graphql/graphql.dart';
import 'package:rodofret/keys.dart';
import 'package:rodofret/pages/freight/freight-create-page/+state/freight-create-page-actions.dart';

class FreightCreateMiddleware extends MiddlewareClass {
  String createFreightMutation = """
    mutation createFreight(\$createFreightInput: CreateFreightInput) {
      createFreight(createFreightInput: \$createFreightInput) {
        code
        message
        data {
          id
          name
          description
          user {
            name
          }
        }
      }
    }
  """
      .replaceAll('\n', ' ');

  @override
  void call(Store store, action, NextDispatcher next) {
    if (action is FreightCreate) {
      mutation(createFreightMutation, variables: {
        'createFreightInput': {
          'name': action.freight['name'],
          'description': action.freight['description'],
          'user_id': 1 //TODO: store.state.loginState.id
        }
      }).map((dynamic data) {
        store.dispatch(new FreightCreated(data));
      }).listen(
        print,
        onDone: () => print('Done!'),
      );
    }

    if (action is FreightCreated) {
      Keys.navKey.currentState.pushNamed("freight-list-page");
    }

    next(action);
  }
}
