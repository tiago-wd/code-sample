import 'package:redux/redux.dart';
import 'package:rodofret/pages/freight/freight-create-page/+state/freight-create-page-actions.dart';
import 'package:rodofret/pages/freight/freight-create-page/+state/freight-create-page-state.dart';

final freightCreateReducer = combineReducers<FreightCreateState>([
  TypedReducer<FreightCreateState, FreightCreate>(_spotCreate),
]);

FreightCreateState _spotCreate(FreightCreateState state, FreightCreate action) => state;
