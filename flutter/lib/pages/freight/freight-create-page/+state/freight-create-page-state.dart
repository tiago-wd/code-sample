import 'package:meta/meta.dart';

@immutable
class FreightCreateState {
  final freight;

  FreightCreateState({
    @required this.freight,
  });

  factory FreightCreateState.initial() {
    return new FreightCreateState(
      freight: null,
    );
  }

  FreightCreateState copyWith({
    freight,
  }) {
    return new FreightCreateState(
      freight: freight ?? this.freight,
    );
  }
}
