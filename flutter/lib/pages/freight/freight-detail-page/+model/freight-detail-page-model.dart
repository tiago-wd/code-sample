import 'package:redux/redux.dart';
import 'package:rodofret/app-state.dart';

class FreightDetailPageModel {
  final getFreight;

  FreightDetailPageModel({
    this.getFreight,
  });

  static FreightDetailPageModel fromStore(Store<AppState> store) => new FreightDetailPageModel(
        getFreight: store.state.freightListState.selectedFreight,
      );
}
