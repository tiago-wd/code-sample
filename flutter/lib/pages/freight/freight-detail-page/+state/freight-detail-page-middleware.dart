import 'package:redux/redux.dart';
import 'package:rodofret/graphql/graphql.dart';
import 'package:rodofret/pages/freight/freight-detail-page/+state/freight-detail-page-actions.dart';

class FreightDetailMiddleware extends MiddlewareClass {
  String getFreightQuery = """
    query getFreight(\$id: Int!) {
      getFreight(id: \$id) {
        code
        data {
          id
          nome
          descricao
          usuario {
            nome
            email
          }
        }
      }
    }
  """
      .replaceAll('\n', ' ');

  @override
  void call(Store store, action, NextDispatcher next) {
    if (action is GetFreight) {
      query(getFreightQuery).map((dynamic data) {
        print(data);
      }).listen(
        print,
        onDone: () => print('Done!'),
      );
    }

    next(action);
  }
}
