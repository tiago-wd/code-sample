import 'package:meta/meta.dart';

@immutable
class FreightDetailState {
  final dynamic freight;

  FreightDetailState({
    @required this.freight,
  });

  factory FreightDetailState.initial() {
    return new FreightDetailState(
      freight: null,
    );
  }

  FreightDetailState copyWith({
    freight,
  }) {
    return new FreightDetailState(
      freight: freight ?? this.freight,
    );
  }
}
