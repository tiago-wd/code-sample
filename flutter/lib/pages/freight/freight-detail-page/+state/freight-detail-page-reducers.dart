import 'package:redux/redux.dart';
import 'package:rodofret/pages/freight/freight-detail-page/+state/freight-detail-page-actions.dart';
import 'package:rodofret/pages/freight/freight-detail-page/+state/freight-detail-page-state.dart';

final freightDetailReducer = combineReducers<FreightDetailState>([
  TypedReducer<FreightDetailState, GetFreight>(_getFreight),
]);

FreightDetailState _getFreight(FreightDetailState state, GetFreight action) => state;
