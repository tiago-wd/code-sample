import 'package:rodofret/graphql/graphql.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:rodofret/app-state.dart';
import 'package:rodofret/graphql/subscription/subscription.dart';
import 'package:rodofret/pages/components/drawer/drawer-widget.dart';
import 'package:rodofret/pages/freight/freight-detail-page/+model/freight-detail-page-model.dart';
import 'package:rxdart/rxdart.dart';

class FreightDetailPage extends StatefulWidget {
  static String tag = 'freight-detail-page';
  _FreightDetailPageState createState() => _FreightDetailPageState();
}

Widget auctionButton(auction, updateInput) {
  if (auction['negociation'] == null) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          RaisedButton(
            onPressed: () => updateAuction(auction, updateInput),
            padding: const EdgeInsets.all(8.0),
            textColor: Colors.white,
            color: Colors.red,
            child: new Text("Não aceitar"),
          ),
          RaisedButton(
            onPressed: () => updateAuction(auction, updateInput),
            padding: const EdgeInsets.all(8.0),
            textColor: Colors.white,
            color: Colors.green,
            child: new Text("Aceitar"),
          ),
        ],
      ),
    );
  } else {
    return Container(
      child: Text('Escolhido'),
    );
  }
}

void updateAuction(auction, updateInput) {
  String updateAuctionInput = """
    mutation updateAuction(\$updateAuctionInput: UpdateAuctionInput) {
      updateAuction(updateAuctionInput: \$updateAuctionInput) {
        code
        message
        data {
          id
          freight {
            name
          }
          user {
            name
          }
          driver {
            name
          }
        }
      }
    }
  """
      .replaceAll('\n', ' ');
  mutation(updateAuctionInput, variables: {
    'updateAuctionInput': {'id': auction['id'], 'negociation': 'accept', 'status': 'ok'}
  }).map((dynamic data) {
    print('save auction');
  }).listen(
    print,
    onDone: () => print('Done!'),
  );
}

class _FreightDetailPageState extends State<FreightDetailPage> {
  Future<SocketClient> socketClient = SocketClient.connect('ws://192.168.3.50:3000/graphql');

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, FreightDetailPageModel>(
      converter: (store) => FreightDetailPageModel.fromStore(store),
      builder: (context, freightDetailModel) {
        final userId = StoreProvider.of<AppState>(context).state.loginState.id;
        final freight = freightDetailModel.getFreight;
        print(freight);
        final updateInput = {'freightId': freight['id'], 'userId': userId};
        final String operationName = "createAuction";
        final String query = """
          subscription $operationName(\$createdAuctionSubscription: CreatedAuctionSubscription!) {
            createAuction(
              createdAuctionSubscription: \$createdAuctionSubscription
            ) {
              code
              message
              data {
                id
                price
                negociation
                status
                driver {
                  name
                }
              }
            }
          }
          """
            .replaceAll('\n', ' ');

        final dynamic variables = {
          'createdAuctionSubscription': {'freight_id': freight['id'], 'user_id': 1}
        };

        final payload = {
          'operationName': operationName,
          'query': query,
          'variables': variables,
        };
        // socketClient.then((ws) => ws.subscribe(payload)).then((ws) => ws.listen((sub) {
        //       print(sub.data['createAuction']);
        //       freight['auctions'].add(sub.data['createAuction']);
        //     }));
        return Scaffold(
          drawer: DrawerWidget(),
          appBar: new AppBar(
            title: new Text('Rodofret'),
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(padding: const EdgeInsets.all(10.0)),
              Text(freight['name'], style: new TextStyle(fontSize: 30.0)),
              Padding(padding: const EdgeInsets.all(10.0)),
              Text(freight['description']),
              Padding(padding: const EdgeInsets.all(10.0)),
              Text("NEGOCIAÇÔES", style: new TextStyle(fontSize: 20.0)),
              Padding(padding: const EdgeInsets.all(10.0)),
              Divider(height: 30),
              Expanded(
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: freight['auctions'] == null ? 0 : freight['auctions'].length,
                  itemBuilder: (BuildContext context, int index) {
                    return new Card(
                      elevation: 8.0,
                      margin: new EdgeInsets.symmetric(horizontal: 1.0, vertical: 1.0),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Column(
                          children: <Widget>[
                            Padding(padding: const EdgeInsets.all(10.0)),
                            Text("ID: " + index.toString()),
                            Text("Preço: " + freight['auctions'][index]['price'].toString()),
                            // Text("Negociação: " + freight['auctions'][index]['negociation']),
                            Padding(padding: const EdgeInsets.all(10.0)),
                            auctionButton(freight['auctions'][index], updateInput),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
