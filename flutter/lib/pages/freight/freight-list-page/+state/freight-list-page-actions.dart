class GetAllFreights {}

class CreateFreightPage {}

class GetAllFreightsSuccess {
  final freights;
  GetAllFreightsSuccess(this.freights);
}

class SelectFreight {
  final freight;
  SelectFreight(this.freight);
}
