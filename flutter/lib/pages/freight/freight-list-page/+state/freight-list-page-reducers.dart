import 'package:redux/redux.dart';
import 'package:rodofret/pages/freight/freight-list-page/+state/freight-list-page-actions.dart';
import 'package:rodofret/pages/freight/freight-list-page/+state/freight-list-page-state.dart';

final freightListReducer = combineReducers<FreightListState>([
  TypedReducer<FreightListState, GetAllFreights>(_getAllFreights),
  TypedReducer<FreightListState, GetAllFreightsSuccess>(_getAllFreightsSuccess),
  TypedReducer<FreightListState, SelectFreight>(_selectFreight),
  TypedReducer<FreightListState, CreateFreightPage>(_createFregithPage),
]);

FreightListState _getAllFreights(FreightListState state, GetAllFreights action) => state;
FreightListState _getAllFreightsSuccess(FreightListState state, GetAllFreightsSuccess action) {
  return state.copyWith(freights: action.freights);
}

FreightListState _selectFreight(FreightListState state, SelectFreight action) {
  return state.copyWith(selectedFreight: action.freight);
}

FreightListState _createFregithPage(FreightListState state, CreateFreightPage action) => state;
