import 'package:redux/redux.dart';
import 'package:rodofret/graphql/graphql.dart';
import 'package:rodofret/keys.dart';
import 'package:rodofret/pages/freight/freight-list-page/+state/freight-list-page-actions.dart';

class FreightListMiddleware extends MiddlewareClass {
  String getAllFreightQuery = """
    query getAllFreights {
      getAllFreights {
        code
        message
        data {
          id
          name
          description
          auctions {
            id
            driver {
              name
            }
            price
            negociation
            status
          }
          user {
            name
            email
          }
        }
      }
    }
  """
      .replaceAll('\n', ' ');

  String getFreightQuery = """
    query getFreight(\$id: Int!) {
      getFreight(id: \$id) {
        code
        data {
          id
          nome
          descricao
          usuario {
            nome
            email
          }
        }
      }
    }
  """
      .replaceAll('\n', ' ');

  @override
  void call(Store store, action, NextDispatcher next) {
    if (action is GetAllFreights) {
      query(getAllFreightQuery).map((dynamic data) {
        store.dispatch(new GetAllFreightsSuccess(data['getAllFreights']['data']));
      }).listen(
        print,
        onDone: () => print('Done!'),
      );
    }

    if (action is SelectFreight) {
      if (store.state.freightListState.selectedFreight != null) {
        Keys.navKey.currentState.pushNamed("freight-detail-page");
      }
    }

    if (action is CreateFreightPage) {
      Keys.navKey.currentState.pushNamed("freight-create-page");
    }

    next(action);
  }
}
