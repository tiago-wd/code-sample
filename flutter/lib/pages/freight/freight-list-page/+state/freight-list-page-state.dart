import 'package:meta/meta.dart';

@immutable
class FreightListState {
  final dynamic freights;
  final selectedFreight;

  FreightListState({
    @required this.freights,
    @required this.selectedFreight,
  });

  factory FreightListState.initial() {
    return new FreightListState(
      freights: null,
      selectedFreight: null,
    );
  }

  FreightListState copyWith({
    freights,
    selectedFreight,
  }) {
    return new FreightListState(
      freights: freights ?? this.freights,
      selectedFreight: selectedFreight ?? this.selectedFreight,
    );
  }
}
