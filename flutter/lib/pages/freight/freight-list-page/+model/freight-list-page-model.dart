import 'package:redux/redux.dart';
import 'package:rodofret/app-state.dart';
import 'package:rodofret/pages/freight/freight-list-page/+state/freight-list-page-actions.dart';

class FreightListPageModel {
  final getAllFreights;
  final Function(dynamic freight) selectFreight;
  final selectedFreight;
  final Function() createFreightPage;

  FreightListPageModel({
    this.getAllFreights,
    this.selectFreight,
    this.selectedFreight,
    this.createFreightPage,
  });

  static FreightListPageModel fromStore(Store<AppState> store) => new FreightListPageModel(
        getAllFreights: store.state.freightListState.freights,
        selectFreight: (freight) => store.dispatch(new SelectFreight(freight)),
        selectedFreight: store.state.freightListState.selectedFreight,
        createFreightPage: () => store.dispatch(new CreateFreightPage()),
      );
}
