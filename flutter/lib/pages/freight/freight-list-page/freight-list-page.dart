import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:rodofret/app-state.dart';
import 'package:rodofret/pages/components/drawer/drawer-widget.dart';
import 'package:rodofret/pages/freight/freight-list-page/+model/freight-list-page-model.dart';
import 'package:rodofret/pages/freight/freight-list-page/+state/freight-list-page-actions.dart';

class FreightListPage extends StatefulWidget {
  static String tag = 'freight-list-page';
  _FreightListPageState createState() => _FreightListPageState();
}

class _FreightListPageState extends State<FreightListPage> {
  final freightListTile = (index, freights, selectFreight) => ListTile(
      contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
      isThreeLine: true,
      leading: Container(
        padding: EdgeInsets.only(right: 12.0),
        decoration: new BoxDecoration(border: new Border(right: new BorderSide(width: 1.0, color: Colors.black))),
        child: Container(
          padding: const EdgeInsets.all(20.0),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.green,
          ),
          child: Icon(Icons.location_on, color: Colors.black),
        ),
      ),
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            freights[index]['name'],
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
          Text(
            freights[index]['description'],
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
          Text(
            freights[index]['user']['name'],
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ],
      ),
      subtitle: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[],
      ),
      trailing: Icon(Icons.keyboard_arrow_right, color: Colors.black, size: 30.0),
      onTap: () => selectFreight(freights[index]));

  final freightCard = (index, freightListTile, freights, selectFreight) => Card(
        elevation: 8.0,
        margin: new EdgeInsets.symmetric(horizontal: 1.0, vertical: 1.0),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
          ),
          child: freightListTile(index, freights, selectFreight),
        ),
      );

  final freightBody = (freightCard, freightListTile, freights, selectFreight) => Container(
      child: freights != null
          ? ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: freights == null ? 0 : freights.length,
              itemBuilder: (BuildContext context, int index) {
                return freightCard(index, freightListTile, freights, selectFreight);
              },
            )
          : Center(
              child: CircularProgressIndicator(),
            ));

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, FreightListPageModel>(
        converter: (store) => FreightListPageModel.fromStore(store),
        onInit: (store) => store.dispatch(GetAllFreights()),
        builder: (context, freightListModel) {
          return Scaffold(
            drawer: DrawerWidget(),
            appBar: new AppBar(
              title: new Text('Rodofret'),
            ),
            body: freightBody(
              freightCard,
              freightListTile,
              freightListModel.getAllFreights == null ? null : freightListModel.getAllFreights.toList(),
              freightListModel.selectFreight,
            ),
            floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
            floatingActionButton: FloatingActionButton(
              child: const Icon(Icons.add),
              backgroundColor: Colors.green,
              foregroundColor: Colors.white,
              onPressed: () => freightListModel.createFreightPage(),
            ),
          );
        });
  }
}
