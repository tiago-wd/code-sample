import 'package:flutter/material.dart';
import 'package:rodofret/pages/components/app-bar-widget/app-bar-widget.dart';
import 'package:rodofret/pages/components/drawer/drawer-widget.dart';

class HomePage extends StatefulWidget {
  static String tag = 'home-page';
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  static String tag = 'home-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: DrawerWidget(),
        appBar: new AppBar(
          title: new Text('Rodofret'),
          actions: <Widget>[
            new PopupMenuButton(itemBuilder: (BuildContext context) {
              return pages.map((Page page) {
                return new PopupMenuItem(
                  value: page,
                  child: new ListTile(
                    title: page.title,
                  ),
                );
              }).toList();
            })
          ],
        ));
  }
}
