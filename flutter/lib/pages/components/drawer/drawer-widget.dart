import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:rodofret/app-state.dart';
import 'package:rodofret/pages/components/drawer/+model/drawer-model.dart';

class DrawerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, DrawerModel>(
      converter: (store) => DrawerModel.fromStore(store),
      builder: (context, drawerModel) {
        return Drawer(
          child: ListView(
            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                child: Text('Informações'),
                decoration: BoxDecoration(
                  color: Colors.green,
                ),
              ),
              ListTile(
                title: Text('Freight'),
                onTap: () => drawerModel.freightPage(),
              )
            ],
          ),
        );
      },
    );
  }
}
