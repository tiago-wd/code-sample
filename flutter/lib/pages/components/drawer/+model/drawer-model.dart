import 'package:redux/redux.dart';
import 'package:rodofret/app-state.dart';
import 'package:rodofret/pages/components/drawer/+state/drawer-widget-actions.dart';

class DrawerModel {
  final Function() freightPage;

  DrawerModel({
    this.freightPage,
  });

  static DrawerModel fromStore(Store<AppState> store) => new DrawerModel(
        freightPage: () => store.dispatch(new FreightPage()),
      );
}
