import 'package:redux/redux.dart';
import 'package:rodofret/keys.dart';
import 'package:rodofret/pages/components/drawer/+state/drawer-widget-actions.dart';

class DrawerMiddleware extends MiddlewareClass {
  @override
  void call(Store store, action, NextDispatcher next) {
    if (action is FreightPage) {
      Keys.navKey.currentState.pushNamed("freight-list-page");
    }

    next(action);
  }
}
