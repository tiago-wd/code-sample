import 'package:meta/meta.dart';

@immutable
class DrawerState {
  DrawerState();

  factory DrawerState.initial() {
    return new DrawerState();
  }

  DrawerState copyWith() {
    return new DrawerState();
  }
}
