import 'package:redux/redux.dart';
import 'package:rodofret/pages/components/drawer/+state/drawer-widget-actions.dart';
import 'package:rodofret/pages/components/drawer/+state/drawer-widget-state.dart';

final drawerReducer = combineReducers<DrawerState>([
  TypedReducer<DrawerState, FreightPage>(_loginPageNavigate),
]);

DrawerState _loginPageNavigate(DrawerState state, FreightPage action) => state;
