import 'package:meta/meta.dart';

@immutable
class AppBarState{

  final index;

  AppBarState({
    @required this.index,
  });

  factory AppBarState.initial(){
    return new AppBarState(
      index: 0,
    );
  }

  AppBarState copyWith({
    index,
  }) {
    return new AppBarState(
      index: index ?? this.index,
    );
  }
}