import 'package:redux/redux.dart';
import 'package:rodofret/keys.dart';
import 'package:rodofret/pages/components/app-bar-widget/+state/app-bar-widget-actions.dart';

class AppBarMiddleware extends MiddlewareClass {
  @override
  void call(Store store, action, NextDispatcher next) {
    if (action is SelectPage) {}

    // if (action is SpotCadastroPage) {
    //   Keys.navKey.currentState.pushNamed("spot-page");
    // }
    next(action);
  }
}
