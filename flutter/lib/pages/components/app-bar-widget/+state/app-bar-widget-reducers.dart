import 'package:redux/redux.dart';
import 'package:rodofret/pages/components/app-bar-widget/+state/app-bar-widget-actions.dart';
import 'package:rodofret/pages/components/app-bar-widget/+state/app-bar-widget-state.dart';

final appBarReducer = combineReducers<AppBarState>([
  TypedReducer<AppBarState, SelectPage>(_selectPage),
  TypedReducer<AppBarState, SpotCadastroPage>(_spotCadastroPage),
]);

AppBarState _selectPage(AppBarState state, SelectPage action) => state.copyWith(index: action.index);
AppBarState _spotCadastroPage(AppBarState state, SpotCadastroPage action) => state;
