import 'package:redux/redux.dart';
import 'package:rodofret/app-state.dart';
import 'package:rodofret/pages/components/app-bar-widget/+state/app-bar-widget-actions.dart';

class AppBarModel {
  final Function(int index) selectPage;
  final Function() spotCadastroPage;
  final selectedPage;

  AppBarModel({
    this.selectPage,
    this.spotCadastroPage,
    this.selectedPage,
  });

  static AppBarModel fromStore(Store<AppState> store) => new AppBarModel(
        selectPage: (index) => store.dispatch(new SelectPage(index)),
        spotCadastroPage: () => store.dispatch(new SpotCadastroPage()),
        selectedPage: store.state.appBarState.index,
      );
}
