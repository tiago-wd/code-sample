import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:rodofret/app-state.dart';
import 'package:rodofret/pages/components/app-bar-widget/+model/app-bar-model.dart';

class Page {
  final int index;
  final Text title;
  final Widget widget;
  final Icon icon;
  const Page({this.index, this.title, this.widget, this.icon});
}

List<Page> pages = <Page>[
  Page(
    index: 0,
    title: const Text('INÍCIO', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18)),
    widget: new Text('INÍCIO'),
  ),
  Page(
    index: 1,
    title: const Text('SPOTS', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18)),
    widget: new Text('SPOTS'),
  ),
  Page(
    index: 2,
    title: const Text('HISTÓRICO', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18)),
    widget: new Text('HISTÓRICO'),
  ),
];

class AppBarWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new AppBarWidgetState();
  }
}

class AppBarWidgetState extends State<AppBarWidget> with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    _tabController = new TabController(length: pages.length, vsync: this);
  }

  void _selectedPage(index) {
    setState(() {
      _tabController.index = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppBarModel>(
        converter: (store) => AppBarModel.fromStore(store),
        builder: (context, appBarModel) {
          return new Scaffold(
            appBar: new AppBar(
              title: new Text('Together'),
              actions: <Widget>[
                new PopupMenuButton(
                  itemBuilder: (BuildContext context) {
                    return pages.map((Page page) {
                      return new PopupMenuItem(
                        value: page,
                        child: new ListTile(
                          title: page.title,
                        ),
                      );
                    }).toList();
                  },
                  onSelected: _selectedPage,
                )
              ],
              bottom: new TabBar(
                controller: _tabController,
                tabs: [
                  new Tab(
                    text: pages[0].title.data,
                  ),
                  new Tab(
                    text: pages[1].title.data,
                  ),
                  new Tab(
                    text: pages[2].title.data,
                  ),
                ],
              ),
            ),
            drawer: new Drawer(
              child: new ListView(
                children: <Widget>[
                  new ListTile(
                    title: pages[0].title,
                    onTap: () {
                      _selectedPage(0);
                      Navigator.pop(context);
                    },
                  ),
                  new ListTile(
                    title: pages[1].title,
                    onTap: () {
                      _selectedPage(1);
                      Navigator.pop(context);
                    },
                  ),
                  new ListTile(
                    title: pages[2].title,
                    onTap: () {
                      _selectedPage(2);
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ),
            body: new TabBarView(
              controller: _tabController,
              children: pages.map((Page page) {
                return page.widget;
              }).toList(),
            ),
            floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
            floatingActionButton: FloatingActionButton(
              child: const Icon(Icons.add),
              backgroundColor: Colors.purple,
              foregroundColor: Colors.white,
              onPressed: () => appBarModel.spotCadastroPage(),
            ),
          );
        });
  }
}
