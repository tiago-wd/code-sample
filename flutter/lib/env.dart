import 'package:meta/meta.dart';

enum BuildFlavor { master, develop, stage, local }

BuildEnvironment get env => _env;
BuildEnvironment _env;

class BuildEnvironment {
  final String baseUrl;
  final String baseUrlGraphql;
  final BuildFlavor flavor;

  BuildEnvironment._init({this.flavor, this.baseUrl, this.baseUrlGraphql});

  getBaseUrl() => baseUrl;

  getBaseUrlGraphql() => baseUrlGraphql;

  static void init({@required flavor, @required baseUrl, @required baseUrlGraphql}) =>
      _env ??= BuildEnvironment._init(flavor: flavor, baseUrl: baseUrl, baseUrlGraphql: baseUrlGraphql);
}
