import 'package:flutter/material.dart';

final inputDecoration = (String hintText) => InputDecoration(
      hintText: hintText,
      contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
    );
