import 'package:meta/meta.dart';
import 'package:rodofret/pages/register-page/+state/register-state.dart';
import 'package:rodofret/pages/components/app-bar-widget/+state/app-bar-widget-state.dart';
import 'package:rodofret/pages/components/drawer/+state/drawer-widget-state.dart';
import 'package:rodofret/pages/login-page/+state/login-state.dart';
import 'package:rodofret/pages/freight/freight-list-page/+state/freight-list-page-state.dart';
import 'package:rodofret/pages/freight/freight-detail-page/+state/freight-detail-page-state.dart';
import 'package:rodofret/pages/freight/freight-create-page/+state/freight-create-page-state.dart';

@immutable
class AppState {
  final LoginState loginState;
  final DrawerState drawerState;
  final RegisterState registerState;
  final AppBarState appBarState;
  final FreightListState freightListState;
  final FreightDetailState freightDetailState;
  final FreightCreateState freightCreateState;

  AppState({
    @required this.loginState,
    @required this.drawerState,
    @required this.registerState,
    @required this.appBarState,
    @required this.freightListState,
    @required this.freightDetailState,
    @required this.freightCreateState,
  });

  factory AppState.initial() {
    return AppState(
      loginState: LoginState.initial(),
      drawerState: DrawerState.initial(),
      registerState: RegisterState.initial(),
      appBarState: AppBarState.initial(),
      freightListState: FreightListState.initial(),
      freightDetailState: FreightDetailState.initial(),
      freightCreateState: FreightCreateState.initial(),
    );
  }

  AppState copyWith({
    LoginState loginState,
    DrawerState drawerState,
    RegisterState registerState,
    AppBarState appBarState,
    FreightListState freightListState,
    FreightDetailState freightDetailState,
    FreightCreateState freightCreateState,
  }) {
    return AppState(
      loginState: loginState ?? this.loginState,
      drawerState: drawerState ?? this.drawerState,
      registerState: registerState ?? this.registerState,
      appBarState: appBarState ?? this.appBarState,
      freightListState: freightListState ?? this.freightListState,
      freightDetailState: freightDetailState ?? this.freightDetailState,
      freightCreateState: freightCreateState ?? this.freightCreateState,
    );
  }
}
