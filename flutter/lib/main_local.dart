import 'package:flutter/material.dart';
import 'package:rodofret/main.dart';
import 'env.dart';

void main() {
  BuildEnvironment.init(
    flavor: BuildFlavor.develop,
    // baseUrl: 'http://192.168.1.103:3000', 192.168.3.50
    baseUrl: 'http://192.168.3.50:3000',
    baseUrlGraphql: 'http://192.168.3.50:3000/graphql',
  );
  assert(env != null);
  runApp(RodofretApp());
}
