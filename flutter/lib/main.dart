import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:rodofret/app-state.dart';
import 'package:rodofret/env.dart';
import 'package:rodofret/keys.dart';

import 'package:rodofret/pages/login-page/login.dart';
import 'package:rodofret/pages/register-page/register.dart';
import 'package:rodofret/pages/home-page/home-page.dart';
import 'package:rodofret/pages/freight/freight.dart';

import 'package:rodofret/pages/components/app-bar-widget/+state/app-bar-widget-middleware.dart';
import 'package:rodofret/pages/components/drawer/+state/drawer-widget-middleware.dart';

import 'package:flutter_redux/flutter_redux.dart';
import 'package:rodofret/app-reducers.dart';

class RodofretApp extends StatelessWidget {
  final routes = <String, WidgetBuilder>{
    LoginPage.tag: (context) => LoginPage(),
    RegisterPage.tag: (context) => RegisterPage(),
    HomePage.tag: (context) => HomePage(),
    FreightListPage.tag: (context) => FreightListPage(),
    FreightDetailPage.tag: (context) => FreightDetailPage(),
    FreightCreatePage.tag: (context) => FreightCreatePage(),
  };

  final store = new Store<AppState>(
    reducers,
    initialState: AppState.initial(),
    middleware: [
      LoginMiddleware(),
      DrawerMiddleware(),
      RegisterMiddleware(),
      AppBarMiddleware(),
      FreightListMiddleware(),
      FreightDetailMiddleware(),
      FreightCreateMiddleware(),
    ],
  );

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: new MaterialApp(
        title: 'Rodofret',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.green,
          fontFamily: 'Roboto',
        ),
        navigatorKey: Keys.navKey,
        home: LoginPage(),
        routes: routes,
      ),
    );
  }
}
