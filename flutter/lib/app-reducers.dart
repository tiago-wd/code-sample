import 'package:rodofret/app-state.dart';
import 'package:rodofret/pages/login-page/+state/login-reducers.dart';
import 'package:rodofret/pages/components/drawer/+state/drawer-widget-reducers.dart';
import 'package:rodofret/pages/register-page/+state/register-reducers.dart';
import 'package:rodofret/pages/components/app-bar-widget/+state/app-bar-widget-reducers.dart';
import 'package:rodofret/pages/freight/freight-list-page/+state/freight-list-page-reducers.dart';
import 'package:rodofret/pages/freight/freight-detail-page/+state/freight-detail-page-reducers.dart';
import 'package:rodofret/pages/freight/freight-create-page/+state/freight-create-page-reducers.dart';

AppState reducers(AppState state, dynamic action) => new AppState(
      loginState: loginReducer(state.loginState, action),
      drawerState: drawerReducer(state.drawerState, action),
      registerState: registerReducer(state.registerState, action),
      appBarState: appBarReducer(state.appBarState, action),
      freightListState: freightListReducer(state.freightListState, action),
      freightDetailState: freightDetailReducer(state.freightDetailState, action),
      freightCreateState: freightCreateReducer(state.freightCreateState, action),
    );
