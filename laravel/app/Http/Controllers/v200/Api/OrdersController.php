<?php

namespace App\Http\Controllers\v200\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\ProcessOrder;
use App\Models\User;
use App\Models\Orders;
use App\Models\CreditCards;
use App\Models\CreditCardBlockeds;
use App\Http\Controllers\v100\Api\FinancialInstitutionController;
use App\Http\Controllers\v100\Api\UsersController;
use App\Http\Controllers\v100\Repositories\OrdersRepository;
use App\Http\Controllers\v100\Repositories\UsersRepository;
use App\Http\Controllers\v200\Api\CreditCardsController;
use App\Classes\EnumBlockedReason;

class OrdersController extends Controller
{
    public function register(Request $request)
    {   
        $params = json_decode($request->getContent(), true);

        $user_id = UsersController::getUserIdByToken($params['user_token'], $params['token']);
        
        if(gettype($user_id) == "object")
            return response($user_id->original, 404);

        $params['client_id'] = $user_id;

        $company_id = UsersController::getCompanyByToken($params['token']);

        if(gettype($company_id) == "object") {
            return response($user_id->original, 404);
        }

        $params['company_id'] = $company_id;

        if(empty($params['payment_type_id']))
            return response('Forma de pagamento não enviada.', 404);

        if(empty($params['credit_card_id']) && empty($params['card_number']))
            return response('Dados do cartão não enviado.', 404);

        if(empty($params['installments']))
            $params['installments'] = 1;

        if(!empty($params['card_number'])) {
            if(OrdersRepository::isCreditCardBlocked($params['card_number'])) {
                return response()->json(["success" => false, "message" => "Cartão bloqueado por suspeita de fraude. Por favor entre em contato com o administrador."], 404);
            }
        }
        
        if(!empty($params['credit_card_id'])) {
            if(OrdersRepository::isCreditCardIdBlocked($params['credit_card_id'])) {
                return response()->json(["success" => false, "message" => "Cartão bloqueado por suspeita de fraude. Por favor entre em contato com o administrador."], 404);
            }
        }
        
        if(!CreditCards::where('id', $request->input('credit_card_id'))->where('user_id', $params['client_id'])->first()) {
            $card = new CreditCardsController();
            $card = $card->register($request);
            if(!$card->original['success']) {
                return response($card->original, 404);
            } else {
                $params['credit_card_id'] = $card->original['card_id'];
            }
        }

        if(!empty($params['device_token'])) {
            $device_token = User::where('id', $user_id)->first();
            if(!$device_token->device_token) {
                $device_token->device_token = $params['device_token'];
                $device_token->save();
            }

            if(UsersRepository::isDeviceBlocked($params['device_token'])) {
                User::where('id', $user_id)->update(['is_blocked' => 1, 'blocked_reason_id' => EnumBlockedReason::BLOCKED_DEVICE]);
                
                if(!empty($params['card_number']))
                    CreditCardBlockeds::create(['card_number' => $params['card_number'], 'is_blocked' => 1, 'blocked_reason_id' => EnumBlockedReason::BLOCKED_DEVICE]);

                return response()->json(["success" => false, "message" => "Aparelho não permitido no sistema. Contate o administrador."], 404);   
            }
        }

        if(Orders::where('reference', $params['reference'])->first()) {
            return response()->json(["success" => false, "message" => "Referência já cadastrada."], 409);
        }
        
        $params['financial_institution_id'] = FinancialInstitutionController::getFinancialInstitution($params);
        $params['total'] = floatval(str_replace(",", ".", $params['total']));

        $order = Orders::create($params);
        
        ProcessOrder::dispatch($order, $params);

        return response()->json(["success" => true, "message" => 'O pedido está sendo processado', 'status' => 'processing'], 201);
    }
}
