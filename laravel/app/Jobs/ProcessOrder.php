<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use App\Classes\PopUtil;
use App\Classes\EnumOrderStatus;
use App\Classes\ActivityLog;
use App\Models\Orders;
use App\Models\AntiFraud;
use App\Models\CreditCards;
use App\Http\Controllers\v200\Api\UseRedeController as UseRede;
use App\Http\Controllers\v200\Api\CieloController as Cielo;
use App\Http\Controllers\v200\Api\KomerciController as Komerci;
use App\Http\Controllers\v200\Api\KondutoController as Konduto;
use App\Http\Controllers\v200\Api\ClearSaleController as ClearSale;

class ProcessOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $order;
    protected $params;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order, $params)
    {
        $this->order = $order;
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(!empty($this->params['credit_card_id'])) {
            $card_info = CreditCards::where('id', $this->params['credit_card_id'])->where('user_id', $this->params['client_id'])->first();
            $this->params['card_number'] = $card_info->card_info->card_number;
            $this->params['brand_id'] = $card_info->brand_id;
            $this->params['exp_year'] = $card_info->card_info->exp_year;
            $this->params['exp_month'] = $card_info->card_info->exp_month;
            $this->params['card_name'] = $card_info->card_info->card_name;
            $this->params['billing']['social_security'] = !empty($card_info->tax_id) ? $card_info->tax_id : null;
            $this->params['billing']['name'] = !empty($card_info->billing_name) ? $card_info->billing_name : null;
            $this->params['billing']['address']['street'] = !empty($card_info->billing_address) ? $card_info->billing_address : null;
            $this->params['billing']['address']['number'] = !empty($card_info->billing_number) ? $card_info->billing_number : null;
            $this->params['billing']['address']['county'] = !empty($card_info->billing_county) ? $card_info->billing_county : null;
            $this->params['billing']['address']['comp'] = !empty($card_info->billing_complement) ? $card_info->billing_complement : null;
            $this->params['billing']['address']['state'] = !empty($card_info->billing_state) ? $card_info->billing_state : null;
            $this->params['billing']['address']['city'] = !empty($card_info->billing_city) ? $card_info->billing_city : null;
            $this->params['billing']['address']['zipcode'] = !empty($card_info->billing_zip) ? $card_info->billing_zip : null;
            $this->params['billing']['phone']['ddd'] = !empty($card_info->billing_phone) ? substr($card_info->billing_phone, 0, 2) : null;
            $this->params['billing']['phone']['number'] = !empty($card_info->billing_phone) ? substr($card_info->billing_phone, 2) : null;
            $this->params['billing']['birth_date'] = !empty($card_info->billing_birth_date) ? $card_info->billing_birth_date : null;
        }

        if(!empty($this->params['anti_fraud'])) {
            $anti_fraud = AntiFraud::where('slug', $this->params['anti_fraud'])->first();
            $this->order->anti_fraud_id = $anti_fraud->id;
            
            switch($this->params['anti_fraud']) {
                case 'konduto':
                    $this->order = Konduto::analyze($this->order, $this->params);

                    if(PopUtil::isJsonResponse($this->order))
                        return $this->order;
                break;
                case 'clearsale':
                    $this->order = ClearSale::analyze($this->order, $this->params);
                    
                    if(PopUtil::isJsonResponse($this->order))
                        return $this->order;
                break;
            }
        } else {
            $this->order->order_status_id = EnumOrderStatus::APPROVED;
            $this->order->save();
        }

        
        if($this->order->order_status_id == EnumOrderStatus::APPROVED || $this->order->order_status_id == EnumOrderStatus::PENDING) {
            if(empty($this->params['cvc'])) {
                $return = ["success" => false, "message" => "Código de segurança deve ser informado."];
                ActivityLog::errorOrder(
                    $this->params, 
                    new Orders(),
                    $return,
                    "Código de segurança não foi informado na compra.", 
                    $this->params['client_id']
                );
                return response()->json($return, 404);
            }
            
            $credential = config('gateway');
            $rede_token = $this->params['rede_token'];
            $this->params['pv'] = $credential[$this->params['rede_token']]['pv'];
            $this->params['rede_token'] = $credential[$this->params['rede_token']]['token'];
            $this->params['soft_description'] = $this->params['card_description'];
            $this->params['capture'] = ($this->order->order_status_id == EnumOrderStatus::PENDING) ? true : false;
        
            switch($rede_token) {
                case 'dev':
                case 'passepague':
                case 'popapps':
                case 'cristoasnacoes':
                    if($rede_token == 'dev') {
                        $this->params['pv'] = '50079557';
                        $this->params['rede_token'] = '4913bb24a0284954be72c4258e229b86';    
                    }

                    if($this->params['installments'] > 1) {
                        $this->params['rede_token'] = $rede_token;
                        $this->order = Komerci::register($this->order, $this->params);
                    } else {
                        $use_rede = new UseRede();
                        $this->order = $use_rede->register($this->order, $this->params);
                    }

                    if(PopUtil::isJsonResponse($this->order))
                        return $this->order;

                break;
                case 'cielo':
                    $this->order = Cielo::register($this->order, $this->params);

                    if(PopUtil::isJsonResponse($this->order))
                        return $this->order;
                break;
                case 'pagseguro':
                break;
            }
        }

        $data = [
            'tid' => $this->order->tid,
            'nsu' => $this->order->nsu,
            'reference' => $this->order->reference
        ];
        
        if(!empty($this->params['capture']) && $this->params['capture'] == true) {
            $data['status'] = 'review';
        } else {
            if($this->order->order_status_id == EnumOrderStatus::APPROVED) {
                $data['status'] = 'approved';
            } else {
                $data['status'] = 'declined';
            }
        }

        // $curl = curl_init($this->base_url_gateway .'/orders/find');
        if(config('gateway.env') == 'dev') {
            $curl = curl_init("http://dev.app.popticket.com.br/api/notification/payment");
        } else {
            $curl = curl_init("http://app.popticket.com.br/api/notification/payment");
        }

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Version: 2.5.0"));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($curl);
        curl_close($curl);

        Log::info($data);
        Log::info($result);
    }
}
